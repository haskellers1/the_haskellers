import pygame
import sys

#pygame.init()

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800

class Rules:
    def __init__(self, screen):
        self.screen = screen
        self.background_image = pygame.image.load('rules.png')
        self.background_image = pygame.transform.scale(self.background_image, (SCREEN_WIDTH, SCREEN_HEIGHT))

        # Define font and bullet points
        self.font = pygame.font.Font(None, 36)
        self.bullet_points = [
            "3 levels in the game",
            "Level 1 - Pick the correct flashcard",
            "Level 2 - Find error in code.",
            "Level 3 - Complete the code with missing words.",
            "Total 3 lives in level-2 and level-3",
            "If lives or timer runs out player can restart that level",
            "Use navigation bar on top right corner to navigate"
            ]
        self.text_color = (0, 0, 0)  # Black color
        self.start_x = 270
        self.start_y = 170
        self.line_spacing = 65

    def draw(self):
        self.screen.blit(self.background_image, (0, 0))
        self.render_bullet_points()

    def render_bullet_points(self):
        y = self.start_y
        for point in self.bullet_points:
            text_surface = self.font.render(f"• {point}", True, self.text_color)
            self.screen.blit(text_surface, (self.start_x, y))
            y += text_surface.get_height() + self.line_spacing

'''def main():
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("Rules")

    rules = Rules(screen)

    clock = pygame.time.Clock()
    running = True

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        rules.draw()

        pygame.display.flip()
        clock.tick(60)

    pygame.quit()
    sys.exit()

if __name__ == "__main__":
    main()'''

