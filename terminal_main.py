import pygame
import sys
from terminal import MyGame
from terminal2 import Game2
from terminal_rules import Terminal_Rules

class Main:
    def __init__(self, screen):
        self.screen = screen
        self.screen_width, self.screen_height = screen.get_size()

        self.background = pygame.image.load('main.jpg')
        self.background = pygame.transform.scale(self.background, (self.screen_width, self.screen_height))

        self.level1 = pygame.image.load('level1.jpg')
        self.level2 = pygame.image.load('level2.jpg')

        self.level1 = pygame.transform.scale(self.level1, (200, 300))
        self.level2 = pygame.transform.scale(self.level2, (200, 300))

        self.level1_pos = (350, 300)
        self.level2_pos = (650, 300)

        self.dropdown_icon = pygame.image.load('duck_icon.png')
        self.dropdown_icon = pygame.transform.scale(self.dropdown_icon, (50, 50))
        self.menu_open = False

        self.button_width = 200
        self.button_height = 60
        self.button_color = (75, 0, 130)
        self.button_hover_color = (100, 0, 150)
        self.font = pygame.font.Font(None, 48)

        self.menu_width = self.button_width
        self.menu_height = 3 * (self.button_height + 10)
        self.menu_x = (self.screen_width - self.menu_width) // 2
        self.menu_y = (self.screen_height - self.menu_height) // 2

        self.menu_buttons = {
            "Home": pygame.Rect(self.menu_x, self.menu_y, self.button_width, self.button_height),
            "Rules": pygame.Rect(self.menu_x, self.menu_y + self.button_height + 10, self.button_width, self.button_height),
            "Quit": pygame.Rect(self.menu_x, self.menu_y + 2 * (self.button_height + 10), self.button_width, self.button_height)
        }
        self.menu_rects = [rect for rect in self.menu_buttons.values()]

        pygame.mixer.init()
        pygame.mixer.music.load('bg.mp3')
        pygame.mixer.music.play(-1)

        self.click_sound = pygame.mixer.Sound('click.mp3')

    def is_within_bounds(self, pos, rect):
        x, y = pos
        rect_x, rect_y, rect_w, rect_h = rect
        return rect_x <= x <= rect_x + rect_w and rect_y <= y <= rect_y + rect_h

    def draw_level_image(self, image, position):
        self.screen.blit(image, position)

    def crossfade_transition(self, new_background):
        surface = pygame.Surface((self.screen_width, self.screen_height))
        for alpha in range(0, 256, 5):
            self.screen.blit(self.background, (0, 0))
            surface.blit(self.screen, (0, 0))
            surface.set_alpha(255 - alpha)
            self.screen.blit(surface, (0, 0))

            new_surface = pygame.Surface((self.screen_width, self.screen_height))
            new_surface.blit(new_background, (0, 0))
            new_surface.set_alpha(alpha)
            self.screen.blit(new_surface, (0, 0))

            pygame.display.flip()
            pygame.time.delay(10)

    def run_game(self, level):
        pygame.mixer.music.stop()  # Stop background music when starting a new level

        new_background = pygame.Surface((self.screen_width, self.screen_height))
        new_background.fill((0, 0, 0))

        if level == 1:
            game = MyGame(self.screen)
        elif level == 2:
            game = Game2(self.screen)
        else:
            raise ValueError("Invalid level")

        self.crossfade_transition(new_background)
        game.run()

        pygame.mixer.music.play(-1) 

    def handle_home_button_click(self):
        pygame.mixer.music.stop()
        from levels import GameController
        game_controller = GameController(self.screen)
        game_controller.run()

    def handle_rules_button_click(self):
        rules = Terminal_Rules(self.screen)
        rules.draw()
        pygame.display.update()
        pygame.time.delay(5000)

    def handle_menu_click(self, mouse_pos):
        for label, rect in self.menu_buttons.items():
            if self.is_within_bounds(mouse_pos, rect):
                if label == "Home":
                    self.handle_home_button_click()
                elif label == "Rules":
                    self.handle_rules_button_click()
                elif label == "Quit":
                    pygame.quit()
                    sys.exit()

    def draw_menu(self):
        menu_background = pygame.Surface((self.screen_width, self.screen_height))
        menu_background.set_alpha(180)
        menu_background.fill((50, 50, 50))
        self.screen.blit(menu_background, (0, 0))

        for label, rect in self.menu_buttons.items():
            pygame.draw.rect(self.screen, self.button_color, rect)
            text_surface = self.font.render(label, True, (255, 255, 255))
            text_rect = text_surface.get_rect(center=rect.center)
            self.screen.blit(text_surface, text_rect)

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    mouse_pos = event.pos
                    if self.is_within_bounds(mouse_pos, (self.screen_width - 60, 10, 50, 50)):
                        self.menu_open = not self.menu_open
                    elif self.menu_open:
                        self.handle_menu_click(mouse_pos)
                    elif self.is_within_bounds(mouse_pos, (*self.level1_pos, 200, 300)):
                        self.click_sound.play()
                        self.run_game(1)
                    elif self.is_within_bounds(mouse_pos, (*self.level2_pos, 200, 300)):
                        self.click_sound.play()
                        self.run_game(2)
                    elif self.menu_open and not any(rect.collidepoint(mouse_pos) for rect in self.menu_rects):
                        self.menu_open = False

            self.screen.blit(self.background, (0, 0))
            self.draw_level_image(self.level1, self.level1_pos)
            self.draw_level_image(self.level2, self.level2_pos)

            if self.menu_open:
                self.draw_menu()

            self.screen.blit(self.dropdown_icon, (self.screen_width - 60, 10))
            pygame.display.flip()

'''pygame.init()
screen_width, screen_height = 1200, 800
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('TriHask')
main = Main(screen)
main.run()'''

