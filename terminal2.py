import pygame
import sys
import random
import subprocess
import threading
import queue


class Game2:
    def __init__(self, screen):
        self.screen = screen
        self.screen_width = 1200
        self.screen_height = 800
        self.half_width = self.screen_width * 2 // 3
        self.half_height = self.screen_height // 2
        self.clock = pygame.time.Clock()
        self.popup_displayed = False
        self.popup_message = ""
        self.popup_start_time = None
        self.popup_duration = 2

        self.background = pygame.image.load("space.jpg").convert()
        self.heart = pygame.image.load("ship.png").convert_alpha()
        self.bg_image = pygame.image.load("t_bg-1.jpg").convert()
        self.bb_image = pygame.image.load("t_bg-2.jpg").convert()

        self.background = pygame.transform.scale(self.background, (self.half_width, self.half_height))
        self.bg_image = pygame.transform.scale(self.bg_image, (self.half_width, self.half_height))
        self.bb_image = pygame.transform.scale(self.bb_image, (self.half_width, self.half_height))

        self.font = pygame.font.SysFont(None, 36)
        self.box_font = pygame.font.SysFont(None, 24)
        self.terminal_font = pygame.font.SysFont('Courier', 18)

        self.colors = {
            'background': (240, 240, 240),
            'box': (255, 255, 255),
            'box_border': (0, 0, 0),
            'text': (0, 0, 0),
            'highlight': (255, 255, 200),
            'button': (0, 0 , 0),
            'button_text': (255, 255, 255),
            'message_background': (100, 50, 150),
            'message_text': (255, 255, 255),
            'terminal_background': (0, 0, 0),
            'terminal_text': (0, 255, 0),
            'cyan': (0, 255, 255),
            'paragraph_text': (255, 255, 255),
        }

        self.current_paragraph_index = 0
        self.paragraphs = self.load_file("qq2.txt")
        self.box_content = self.load_file("ques2.txt")
        self.setup_paragraph()
        self.boxes = self.create_boxes()
        self.dragging = None
        self.drag_offset = (0, 0)
        self.correct_order = False
        self.box_padding = 20
        self.correct_sound = pygame.mixer.Sound("cor.mp3")
        self.incorrect_sound = pygame.mixer.Sound("try.mp3")
        self.next_button = pygame.Rect(690, self.half_height + self.half_height - 250, 100, 40)
        self.reset_button = pygame.Rect(690, self.half_height + self.half_height - 100, 100, 40)
        self.back_button = pygame.Rect(690, self.half_height + self.half_height - 150, 100, 40)
        self.check_button = pygame.Rect(690, self.half_height + self.half_height - 50, 100, 40)
        self.next_button_visible = False
        self.message = ""
        self.message_start_time = None
        self.message_duration = 2

        self.heart_x_pos = 300
        self.heart_y_pos = 300
        self.heart_moved = False
        self.terminal_input = ""
        self.terminal_output = []
        self.terminal_active = True
        self.terminal_cursor_position = 0
        self.ghci_output_queue = queue.Queue()

        self.ghci_process = subprocess.Popen(
            ['ghci'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True
        )

        # Thread to read GHCi output
        #self.ghci_thread = threading.Thread(target=self.read_ghci_output, daemon=True)
        #self.ghci_thread.start()

    def load_file(self, filename):
        with open(filename, 'r') as file:
            content = file.read()
            paragraphs = [p.strip() for p in content.split('\n\n') if p.strip()]
            return paragraphs

    def setup_paragraph(self):
        if self.current_paragraph_index < len(self.paragraphs):
            self.original_lines = self.split_into_lines(self.box_content[self.current_paragraph_index])
            self.jumbled_lines = self.shuffle_lines(self.original_lines)
            self.boxes = self.create_boxes()
            self.correct_order = False
            self.next_button_visible = False
            self.heart_moved = False
        else:
            print("No more paragraphs available!")

    def split_into_lines(self, content):
        return [line.strip() for line in content.split('\n') if line.strip()]

    def shuffle_lines(self, lines):
        shuffled = lines[:]
        random.shuffle(shuffled)
        return shuffled

    def create_boxes(self):
        box_padding = 20
        start_x = 50
        start_y = self.half_height + 50
        max_width = self.screen_width // 2 - start_x - box_padding

        boxes = []
        x, y = start_x, start_y

        for i, line in enumerate(self.jumbled_lines):
            text_surface = self.box_font.render(line, True, self.colors['text'])
            box_width = text_surface.get_width() + 2 * box_padding
            box_height = text_surface.get_height() + 2 * box_padding

            if box_width > max_width:
                box_width = max_width

            box_rect = pygame.Rect(x, y, box_width, box_height)

            boxes.append({
                "text": line,
                "rect": box_rect,
                "original_position": (x, y)
            })

            y += box_height + box_padding
            if y + box_height > self.screen_height:
                y = start_y
                x += max_width + box_padding
                if x + max_width > self.screen_width:
                    x = start_x

        return boxes

    def display_popup(self):
        if self.popup_message:
            shadow_rect = pygame.Rect(self.half_width // 2 - 155, self.half_height // 2 - 55, 310, 110)
            pygame.draw.rect(self.screen, (230, 230, 250), shadow_rect, border_radius=15)
            popup_rect = pygame.Rect(self.half_width // 2 - 150, self.half_height // 2 - 50, 300, 100)
            pygame.draw.rect(self.screen, (75, 0, 130), popup_rect, border_radius=15)
            pygame.draw.rect(self.screen, (230, 230, 250), popup_rect, 2, border_radius=15)

            text_surface = self.font.render(self.popup_message, True, (255, 255, 255))
            text_rect = text_surface.get_rect(center=popup_rect.center)
            self.screen.blit(text_surface, text_rect)
            button_width = 80
            button_height = 30
            button_padding = 10
            home_button_rect = pygame.Rect(popup_rect.x + button_padding,
                                           popup_rect.bottom - button_height - button_padding, button_width,
                                           button_height)
            back_button_rect = pygame.Rect(popup_rect.right - button_width - button_padding,
                                           popup_rect.bottom - button_height - button_padding, button_width,
                                           button_height)

            mouse_pos = pygame.mouse.get_pos()
            mouse_click = pygame.mouse.get_pressed()

            home_button_color = (128, 0, 128) if home_button_rect.collidepoint(mouse_pos) else (
            160, 32, 240)
            back_button_color = (128, 0, 128) if back_button_rect.collidepoint(mouse_pos) else (
            160, 32, 240)

            pygame.draw.rect(self.screen, home_button_color, home_button_rect, border_radius=10)
            pygame.draw.rect(self.screen, back_button_color, back_button_rect, border_radius=10)

            home_text_surface = self.font.render('Home', True, (255, 255, 255))
            home_text_rect = home_text_surface.get_rect(center=home_button_rect.center)
            self.screen.blit(home_text_surface, home_text_rect)

            back_text_surface = self.font.render('Back', True, (255, 255, 255))
            back_text_rect = back_text_surface.get_rect(center=back_button_rect.center)
            self.screen.blit(back_text_surface, back_text_rect)
            if home_button_rect.collidepoint(mouse_pos) and mouse_click[0]:
                self.handle_home_button_click()
            elif back_button_rect.collidepoint(mouse_pos) and mouse_click[0]:
                self.handle_back_button_click()
    def handle_home_button_click(self):
        print("clicked")
        from levels import GameController
        all = GameController(screen)
        all.run()
    def handle_back_button_click(self):
        print("click")
        from terminal_main import Main
        main = Main(screen)
	#self.crossfade_transition(new_background)
        main.run()

    def draw_boxes(self):
        for box in self.boxes:
            pygame.draw.rect(self.screen, self.colors['box'], box["rect"])
            pygame.draw.rect(self.screen, self.colors['box_border'], box["rect"], 2)
            text_surface = self.box_font.render(box["text"], True, self.colors['text'])
            text_rect = text_surface.get_rect(center=box["rect"].center)
            self.screen.blit(text_surface, text_rect)

    def draw_buttons(self):
        if self.next_button_visible:
            pygame.draw.rect(self.screen, self.colors['button'], self.next_button)
            text_surface = self.font.render('Next', True, self.colors['button_text'])
            text_rect = text_surface.get_rect(center=self.next_button.center)
            self.screen.blit(text_surface, text_rect)

        pygame.draw.rect(self.screen, self.colors['button'], self.reset_button)
        text_surface = self.font.render('Reset', True, self.colors['button_text'])
        text_rect = text_surface.get_rect(center=self.reset_button.center)
        self.screen.blit(text_surface, text_rect)

        pygame.draw.rect(self.screen, self.colors['button'], self.back_button)
        text_surface = self.font.render('Back', True, self.colors['button_text'])
        text_rect = text_surface.get_rect(center=self.back_button.center)
        self.screen.blit(text_surface, text_rect)

        pygame.draw.rect(self.screen, self.colors['button'], self.check_button)
        text_surface = self.font.render('Check', True, self.colors['button_text'])
        text_rect = text_surface.get_rect(center=self.check_button.center)
        self.screen.blit(text_surface, text_rect)

    def display_message(self):
        if self.message and self.message_start_time:
            current_time = pygame.time.get_ticks() / 1000
            elapsed_time = current_time - self.message_start_time
            if elapsed_time < self.message_duration:
                message_rect = pygame.Rect(self.half_width // 2 - 150, self.half_height + 20, 300, 80)
                pygame.draw.rect(self.screen, self.colors['message_background'], message_rect, border_radius=10)
                pygame.draw.rect(self.screen, self.colors['box_border'], message_rect, 2, border_radius=10)
                text_surface = self.font.render(self.message, True, self.colors['message_text'])
                text_rect = text_surface.get_rect(center=message_rect.center)
                self.screen.blit(text_surface, text_rect)
            else:
                self.message = ""
                self.message_start_time = None

    def draw_heart(self):
        heart = pygame.transform.scale(self.heart, (100, 100))
        heart_rect = self.heart.get_rect(topleft=(self.heart_x_pos, self.heart_y_pos))
        self.screen.blit(heart, heart_rect)

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.pos[0] < self.half_width and event.pos[1] >= self.half_height:
                    if self.next_button.collidepoint(event.pos) and self.next_button_visible:
                        self.load_next_paragraph()
                    elif self.reset_button.collidepoint(event.pos):
                        self.reset_boxes()
                    elif self.back_button.collidepoint(event.pos):
                        self.handle_back_button_click()
                    elif self.check_button.collidepoint(event.pos):
                        self.check_answer()
                    else:
                        self.start_drag(event.pos)
            elif event.type == pygame.MOUSEBUTTONUP:
                self.stop_drag()
            elif event.type == pygame.MOUSEMOTION:
                self.drag_box(event.pos)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    if self.terminal_active:
                        self.terminal_input += '\n'
                    else:
                        self.execute_terminal_command()
                elif event.key == pygame.K_BACKSPACE:
                    if self.terminal_active and len(self.terminal_input) > 0:
                        self.terminal_input = self.terminal_input[:-1]
                elif event.key == pygame.K_UP:
                    if self.terminal_cursor_position > 0:
                        self.terminal_cursor_position -= 1
                elif event.key == pygame.K_DOWN:
                    if self.terminal_cursor_position < len(self.terminal_lines):
                        self.terminal_cursor_position += 1
                else:
                    if self.terminal_active:
                        self.terminal_input += event.unicode
    def check_answer(self):
        sorted_boxes = sorted(self.boxes, key=lambda box: (box["rect"].y, box["rect"].x))
        boxes_correct = True
        for i, box in enumerate(sorted_boxes):
            if box["text"] != self.original_lines[i]:
                boxes_correct = False
                break

        terminal_correct = self.check_terminal()

        if boxes_correct or terminal_correct:
            self.correct_sound.play()
            self.message = "Correct!"
            self.message_start_time = pygame.time.get_ticks() / 1000
            self.next_button_visible = True
            if not self.heart_moved:
                self.heart_y_pos -= 100
                self.heart_moved = True
        else:
            self.incorrect_sound.play()
            self.message = "Try Again!"
            self.message_start_time = pygame.time.get_ticks() / 1000

    def check_terminal(self):
        expected_output = self.box_content[self.current_paragraph_index].strip()
        terminal_input = self.terminal_input.strip()
        if terminal_input == expected_output:
            self.terminal_input = ""
            return True
        else:
            return False
    def start_drag(self, mouse_pos):
        for box in self.boxes:
            if box["rect"].collidepoint(mouse_pos):
                self.dragging = box
                self.drag_offset = (box["rect"].x - mouse_pos[0], box["rect"].y - mouse_pos[1])
                break

    def stop_drag(self):
        if self.dragging:
            self.dragging = None

    def drag_box(self, mouse_pos):
        if self.dragging:
            new_x = mouse_pos[0] + self.drag_offset[0]
            new_y = mouse_pos[1] + self.drag_offset[1]
            if new_x < 0:
                new_x = 0
            if new_y < 0:
                new_y = 0
            max_x = self.screen_width - self.dragging["rect"].width
            max_y = self.screen_height - self.dragging["rect"].height

            if new_x > max_x:
                new_x = max_x
            if new_y > max_y:
                new_y = max_y

            self.dragging["rect"].x = new_x
            self.dragging["rect"].y = new_y
    def reset_boxes(self):
        for box in self.boxes:
            x, y = box["original_position"]
            x = max(0, min(x, self.screen_width - box["rect"].width))
            y = max(0, min(y, self.screen_height - box["rect"].height))
            box["rect"].x, box["rect"].y = x, y

    def load_next_paragraph(self):
        self.current_paragraph_index += 1
        self.setup_paragraph()

    def draw_terminal(self):
        terminal_rect = pygame.Rect(self.half_width, self.half_height, self.half_width, self.half_height)
        pygame.draw.rect(self.screen, self.colors['terminal_background'], terminal_rect)
        pygame.draw.rect(self.screen, self.colors['box_border'], terminal_rect, 2)

        input_surface = self.terminal_font.render(f'> {self.terminal_input}', True, self.colors['terminal_text'])
        self.screen.blit(input_surface, (terminal_rect.x + 5, terminal_rect.y + 5))

        output_y = terminal_rect.y + 25
        for i, output in enumerate(self.terminal_output):
            output_surface = self.terminal_font.render(output, True, self.colors['terminal_text'])
            self.screen.blit(output_surface, (terminal_rect.x + 5, output_y + i * 20))

    def execute_terminal_command(self):
        command = self.terminal_input.strip()
        if command:
            self.ghci_process.stdin.write(command + '\n')
            self.ghci_process.stdin.flush()
            output = self.ghci_process.stdout.readline().strip()
            self.terminal_output.append(output)
            if len(self.terminal_output) > 10:
                self.terminal_output.pop(0)

            self.terminal_input = ""

    def render_paragraphs(self):
        if self.current_paragraph_index < len(self.paragraphs):
            lines = self.split_into_lines(self.paragraphs[self.current_paragraph_index])
            y_offset = 10
            for line in lines:
                text_surface = self.box_font.render(line, True, self.colors['paragraph_text'])
                self.screen.blit(text_surface, (self.half_width + 10, y_offset))
                y_offset += text_surface.get_height() + 5

    def run(self):
        while True:
            self.handle_events()

            self.screen.fill(self.colors['background'])

            self.screen.blit(self.background, (0, 0))

            self.screen.blit(self.bg_image, (self.half_width, 0))

            self.render_paragraphs()

            self.screen.blit(self.bb_image, (0, self.half_height))

            self.draw_boxes()

            self.draw_heart()

            self.draw_buttons()

            self.display_message()

            #self.render_terminal_output()
            self.draw_terminal()
            if self.current_paragraph_index >= len(self.paragraphs):
                if not self.popup_message:
                    self.popup_message = "YOU DID IT"
                    self.popup_displayed = True
                self.display_popup()
            else:
                self.draw_terminal()
            pygame.display.flip()
            self.clock.tick(60)

'''pygame.init()'''
screen = pygame.display.set_mode((1200, 800))
'''pygame.display.set_caption("My Pygame Game")
game = Game2(screen)
game.run()'''

