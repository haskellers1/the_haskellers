import pygame
import sys
import time

class after_map:
    def __init__(self, screen):
        self.screen_width, self.screen_height = 1200, 800
        #self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        self.screen = screen
        pygame.display.set_caption("TriHask")
        self.background_image = pygame.image.load('haunted_house.png')
        self.zoom_duration = 5
        self.zoom_start_delay = 2
        self.start_time = time.time()
        self.end_time = self.start_time + self.zoom_duration + self.zoom_start_delay
        self.font = pygame.font.SysFont(None, 48)
        self.initial_message_text = "This should be the location."
        self.final_message_text = "Let's Go then!"
        self.message_displayed = False

    def run(self):
        running = True
        while running:
            current_time = time.time()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            self.screen.fill((0, 0, 0))

            if current_time < self.start_time + self.zoom_start_delay:
                self.screen.blit(self.background_image, (0, 0))
                message_text = self.initial_message_text
            else:
                elapsed_time = current_time - (self.start_time + self.zoom_start_delay)
                if elapsed_time < self.zoom_duration:
                    zoom_factor = 1 + (elapsed_time / self.zoom_duration) * 0.5
                else:
                    zoom_factor = 1.5
                zoomed_width = int(self.screen_width * zoom_factor)
                zoomed_height = int(self.screen_height * zoom_factor)
                zoomed_image = pygame.transform.scale(self.background_image, (zoomed_width, zoomed_height))
                x_offset = (zoomed_width - self.screen_width) // 2
                y_offset = (zoomed_height - self.screen_height) // 2
                self.screen.blit(zoomed_image, (-x_offset, -y_offset))

                if not self.message_displayed and elapsed_time >= 0:
                    self.message_displayed = True
                message_text = self.final_message_text

            rect_width = self.screen_width
            rect_height = 100
            rect_color = (0, 0, 0, 180)
            rect_surface = pygame.Surface((rect_width, rect_height), pygame.SRCALPHA)
            rect_surface.fill(rect_color)
            self.screen.blit(rect_surface, (0, self.screen_height - rect_height))

            text_color = (255, 255, 255)
            text_surface = self.font.render(message_text, True, text_color)
            text_rect = text_surface.get_rect(center=(self.screen_width / 2, self.screen_height - rect_height / 2))
            self.screen.blit(text_surface, text_rect.topleft)

            pygame.display.flip()

            if current_time >= self.end_time:
                running = False
                return False
        #return
        '''pygame.quit()
        sys.exit()'''

'''
pygame.init()
effect = after_map()
effect.run()'''

