import pygame
import sys
from button import Button
from error import Game as ErrorGame

pygame.init()

class Game:
    def __init__(self, screen):
        self.SCREEN_WIDTH = 1200
        self.SCREEN_HEIGHT = 800
        self.QUESTION_POSITION = (150, 300)
        self.BUTTON_POSITION = (500, 450)
        self.BUTTON_TEXT = "Start"
        self.FONT_SIZE = 50
        self.screen = screen
        self.font = pygame.font.SysFont("Inkfree", self.FONT_SIZE)

        self.start_button = Button(self.BUTTON_TEXT, 200, 40, self.BUTTON_POSITION, 5, self.font)

        self.question = "In this level you have to find error in the code snippet."

        self.background_image = pygame.image.load('roombg.jpg')
        self.background_image = pygame.transform.scale(self.background_image, (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))

        self.running = True

    def run(self):
        while self.running:
            self.draw()
            self.handle_events()

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if self.start_button.top_rect.collidepoint(event.pos):
                    print("Start button clicked")
                    game_instance = ErrorGame(self.screen)
                    game_instance.run()
                    self.running = False

    def draw(self):
        self.screen.blit(self.background_image, (0, 0))

        text_surface = self.font.render(self.question, True, pygame.Color('white'))
        self.screen.blit(text_surface, self.QUESTION_POSITION)

        self.start_button.draw(self.screen)

        pygame.display.update()

'''screen = pygame.display.set_mode((1200, 800))
pygame.display.set_caption('Game with Background')

main_menu = Game(screen)
main_menu.run()'''
