import pygame
import sys

class Button:
    pygame.font.init()
    gui_font = pygame.font.Font(None, 30)
    def __init__(self, text, width, height, pos, elevation, gui_font):
        self.pressed = False
        self.elevation = elevation
        self.dynamic_elevation = elevation
        self.original_y_pos = pos[1]
        self.screen = pygame.display.set_mode((1200, 800))

        self.top_rect = pygame.Rect(pos, (width, height))
        self.top_color = '#475F77'

        self.bottom_rect = pygame.Rect(pos, (width, height))
        self.bottom_color = '#354B5E'

        self.text_surf = gui_font.render(text, True, '#FFFFFF')
        self.text_rect = self.text_surf.get_rect(center=self.top_rect.center)

    def draw(self, screen):
        self.top_rect.y = self.original_y_pos - self.dynamic_elevation
        self.text_rect.center = self.top_rect.center

        self.bottom_rect.midtop = self.top_rect.midtop
        self.bottom_rect.height = self.top_rect.height + self.dynamic_elevation

        pygame.draw.rect(screen, self.bottom_color, self.bottom_rect, border_radius=12)
        pygame.draw.rect(screen, self.top_color, self.top_rect, border_radius=12)
        screen.blit(self.text_surf, self.text_rect)
        return self.check_click()

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.top_rect.collidepoint(mouse_pos):
            self.top_color = '#D74B4B'
            if pygame.mouse.get_pressed()[0]:
                self.dynamic_elevation = 0
                self.pressed = True
            else:
                self.dynamic_elevation = self.elevation
                if self.pressed:
                    self.pressed = False
                    return True
        else:
            self.dynamic_elevation = self.elevation
            self.top_color = '#475F77'
        return False


class FlashcardsGame:
    WHITE = (255, 255, 255)
    VIOLET = (127, 0, 255)
    BLACK = (0, 0, 0)
    FLASHCARD_COLOR = (173, 216, 230)
    FLASHCARD_BACK_COLOR_RED = (255, 0, 0)
    FLASHCARD_BACK_COLOR_GREEN = (0, 255, 0)
    FLASHCARD_WIDTH, FLASHCARD_HEIGHT = 300, 200
    FLASHCARD_DEPTH = 20
    SPACING = 40
    SHADOW_COLOR = (255, 255, 255)
    SHADOW_OFFSET = 5
    gui_font = pygame.font.Font(None, 30)
    WIDTH, HEIGHT = 1200, 800

    next_button = Button('Next', 200, 50, (WIDTH // 2 - 100, HEIGHT - 100), 5, gui_font)

    def __init__(self,screen):
        self.flashcard_sets = [
            ["Char", "Double", "Boolean"],
            ["10", "14", "15"],
            [
                "PyHaskell",
                "Glasgow"
                "Haskell Compiler", 
                "CHaskell"
            ]
        ]
        self.question_sets = [
            "Which of the following is not a built-in data type in Haskell?",
            "What will be the output when fac is called with an argument of 5:\n"
            "fac :: Integer -> Integer\n"
            "fac n | n == 0 = 0\n"
            "fac n | n > 0 = n + fac(n-1)",
            "What is the main implementation of Haskell?"
        ]

        self.current_set = 0
        self.sets_shown = 0
        self.flipped = [False, False, False]
        self.auto_flip_timer = None
        self.green_flipped = False
        self.clicked = [False, False, False]

        self.total_width = 3 * self.FLASHCARD_WIDTH + 2 * self.SPACING
        self.start_x = (self.WIDTH - self.total_width) // 2
        if self.start_x < 0:
            self.start_x = 0
        self.screen = screen

        

    def draw_flashcard(self, surface, x, y, color, depth):
        pygame.draw.rect(surface, self.SHADOW_COLOR, (x + self.SHADOW_OFFSET, y + self.SHADOW_OFFSET, self.FLASHCARD_WIDTH, self.FLASHCARD_HEIGHT))
        for i in range(1, depth):
            pygame.draw.line(surface, self.SHADOW_COLOR, (x + i + self.SHADOW_OFFSET, y + self.SHADOW_OFFSET),
                             (x + i + self.SHADOW_OFFSET, y + self.FLASHCARD_HEIGHT + self.SHADOW_OFFSET))
            pygame.draw.line(surface, self.SHADOW_COLOR, (x + self.SHADOW_OFFSET, y + i + self.SHADOW_OFFSET),
                             (x + self.FLASHCARD_WIDTH + self.SHADOW_OFFSET, y + i + self.SHADOW_OFFSET))

        pygame.draw.rect(surface, color, (x, y, self.FLASHCARD_WIDTH, self.FLASHCARD_HEIGHT))

    def fade(self, screen, color, duration=1000):
        fade_surface = pygame.Surface((self.WIDTH, self.HEIGHT))
        fade_surface.fill(color)
        for alpha in range(0, 255):
            fade_surface.set_alpha(alpha)
            screen.blit(fade_surface, (0, 0))
            pygame.display.flip()
            pygame.time.delay(duration // 255)

    def next_flashcards(self):
        self.flipped = [False, False, False]
        self.clicked = [False, False, False]  # Reset clicked status for new screen
        self.current_set += 1
        self.sets_shown += 1
        self.auto_flip_timer = None
        self.green_flipped = False

    def run(self):
        font = pygame.font.Font(None, 36)
        while self.sets_shown < len(self.flashcard_sets):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN and self.auto_flip_timer is None:
                    mouse_x, mouse_y = event.pos
                    for i in range(3):
                        card_x = self.start_x + i * (self.FLASHCARD_WIDTH + self.SPACING)
                        card_y = self.HEIGHT // 2 + 50 - self.FLASHCARD_HEIGHT // 2
                        if (card_x < mouse_x < card_x + self.FLASHCARD_WIDTH and
                                card_y < mouse_y < card_y + self.FLASHCARD_HEIGHT and
                                not self.clicked[i]):
                            self.clicked[i] = True
                            self.flipped[i] = not self.flipped[i]
                            if self.current_set == 1:
                                if i != 1:
                                    self.auto_flip_timer = pygame.time.get_ticks()
                                else:
                                    self.green_flipped = True
                            elif self.current_set == 2:
                                if i != 1:
                                    self.auto_flip_timer = pygame.time.get_ticks()
                                else:
                                    self.green_flipped = True
                            else:
                                if i != 2:
                                    self.auto_flip_timer = pygame.time.get_ticks()
                                else:
                                    self.green_flipped = True

            self.screen.fill(self.BLACK)
            question_lines = self.question_sets[self.current_set].split('\n')
            bold_font = pygame.font.Font(None, 48)
            for index, line in enumerate(question_lines):
                text_surface_bold = bold_font.render(line, True, self.WHITE)
                text_rect_bold = text_surface_bold.get_rect(center=(self.WIDTH // 2, self.HEIGHT // 4 + index * 50 - 50))
                self.screen.blit(text_surface_bold, text_rect_bold)

            for i in range(3):
                card_x = self.start_x + i * (self.FLASHCARD_WIDTH + self.SPACING)
                card_y = self.HEIGHT // 2 + 50 - self.FLASHCARD_HEIGHT // 2
                if self.flipped[i]:
                    if self.current_set == 1:
                        if i == 1:
                            self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_BACK_COLOR_GREEN, self.FLASHCARD_DEPTH)
                            text_surface = font.render("Correct", True, self.BLACK)
                        else:
                            self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_BACK_COLOR_RED, self.FLASHCARD_DEPTH)
                            text_surface = font.render("Incorrect", True, self.BLACK)
                    elif self.current_set == 2:
                        if i == 1:
                            self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_BACK_COLOR_GREEN, self.FLASHCARD_DEPTH)
                            text_surface = font.render("Correct", True, self.BLACK)
                        else:
                            self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_BACK_COLOR_RED, self.FLASHCARD_DEPTH)
                            text_surface = font.render("Incorrect", True, self.BLACK)
                    else:
                        if i == 2:
                            self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_BACK_COLOR_GREEN, self.FLASHCARD_DEPTH)
                            text_surface = font.render("Correct", True, self.BLACK)
                        else:
                            self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_BACK_COLOR_RED, self.FLASHCARD_DEPTH)
                            text_surface = font.render("Incorrect", True, self.BLACK)
                else:
                    self.draw_flashcard(self.screen, card_x, card_y, self.FLASHCARD_COLOR, self.FLASHCARD_DEPTH)
                    text_surface = font.render(self.flashcard_sets[self.current_set][i], True, self.BLACK)
                text_rect = text_surface.get_rect(center=(card_x + self.FLASHCARD_WIDTH // 2, card_y + self.FLASHCARD_HEIGHT // 2))
                self.screen.blit(text_surface, text_rect)

            if self.next_button.draw(self.screen) and self.green_flipped:
                self.next_flashcards()

            pygame.display.flip()

            if self.green_flipped and all(self.flipped):
                self.fade(self.screen, self.BLACK)
                self.next_flashcards()
                if self.sets_shown < len(self.flashcard_sets):
                    self.fade(self.screen, self.BLACK, duration=500)
                self.green_flipped = False

            if self.auto_flip_timer is not None and pygame.time.get_ticks() - self.auto_flip_timer > 2000:
                self.auto_flip_timer = None
                if self.current_set == 1 or self.current_set == 2:
                    self.flipped[1] = True
                    self.green_flipped = True
                else:
                    self.flipped[2] = True
                    self.green_flipped = True

'''pygame.init()
screen = pygame.display.set_mode((1200, 800))
game = FlashcardsGame(screen)
game.run()
pygame.quit()
sys.exit()
 '''
