import pygame
import sys
import time
from lock_phone import WhatsAppPhone

class PhoneLockScreen:
    def __init__(self, screen):
        self.WINDOW_WIDTH, self.WINDOW_HEIGHT = 1200, 800
        #self.screen = pygame.display.set_mode((self.WINDOW_WIDTH, self.WINDOW_HEIGHT))
        pygame.display.set_caption('TriHask')
        self.screen = screen
        self.background_img = pygame.image.load('initial_room2.jpg')
        self.background_img = pygame.transform.scale(self.background_img, (self.WINDOW_WIDTH, self.WINDOW_HEIGHT))

        self.phone_wallpaper = pygame.image.load('phone_bg.jpg')
        self.phone_wallpaper = pygame.transform.scale(self.phone_wallpaper, (400, 800))

        self.PHONE_WIDTH, self.PHONE_HEIGHT = 400, 800
        self.PHONE_X = (self.WINDOW_WIDTH - self.PHONE_WIDTH) // 2
        self.PHONE_Y = (self.WINDOW_HEIGHT - self.PHONE_HEIGHT) // 2

        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)
        self.GREY = (169, 169, 169)
        self.LIGHT_GREY = (200, 200, 200)
        self.TRANSLUCENT_BLACK = (0, 0, 0, 180)
        self.RED = (255, 0, 0)
        self.GREEN = (0, 255, 0)

        self.font_small = pygame.font.Font(None, 36)
        self.font_large = pygame.font.Font(None, 72)
        self.font_tiny = pygame.font.Font(None, 24)
        self.font_extra_large = pygame.font.Font(None, 48)
        self.font_message = pygame.font.Font(None, 72)

        self.notifications = []
        self.notification_start_time = time.time()
        self.all_notifications_shown = False
        self.whatsapp_popup_rect = pygame.Rect(0, 0, 0, 0)
        self.overlay_visible = False
        self.bottom_message_visible = False
        self.phone_visible = True
        self.transition_alpha = 0

    def draw_rounded_rect(self, surface, color, rect, radius):
        x, y, width, height = rect
        pygame.draw.rect(surface, color, rect, border_radius=radius)

    def draw_phone_frame(self):
        pygame.draw.rect(self.screen, self.BLACK, (self.PHONE_X, self.PHONE_Y, self.PHONE_WIDTH, self.PHONE_HEIGHT), 0, border_radius=30)
        pygame.draw.rect(self.screen, self.WHITE, (self.PHONE_X + 10, self.PHONE_Y + 10, self.PHONE_WIDTH - 20, self.PHONE_HEIGHT - 20), 0, border_radius=20)
        pygame.draw.ellipse(self.screen, self.GREY, (self.PHONE_X + self.PHONE_WIDTH // 2 - 30, self.PHONE_Y + self.PHONE_HEIGHT - 80, 60, 10))

    def draw_lock_screen(self):
        self.screen.blit(self.phone_wallpaper, (self.PHONE_X + 10, self.PHONE_Y + 10))
        current_time = time.strftime("%H:%M", time.localtime())
        time_text = self.font_large.render(current_time, True, self.WHITE)
        self.screen.blit(time_text, (self.PHONE_X + (self.PHONE_WIDTH - time_text.get_width()) // 2, self.PHONE_Y + 30))

        y_offset = 100
        notification_height = 80
        border_radius = 15

        for notification, timestamp, color in self.notifications:
            notification_rect = pygame.Rect(self.PHONE_X + 20, self.PHONE_Y + y_offset, 360, notification_height)
            pygame.draw.rect(self.screen, self.TRANSLUCENT_BLACK, notification_rect, border_radius=border_radius)
            pygame.draw.rect(self.screen, self.LIGHT_GREY, notification_rect, 2, border_radius=border_radius)
            label_text = notification.split(": ")[0]
            notification_label_text = self.font_small.render(label_text, True, color)
            self.screen.blit(notification_label_text, (self.PHONE_X + 30, self.PHONE_Y + y_offset + 10))
            content_text = self.font_small.render(notification.split(": ")[1], True, self.WHITE)
            self.screen.blit(content_text, (self.PHONE_X + 30, self.PHONE_Y + y_offset + 30))
            timestamp_text = self.font_tiny.render(timestamp, True, self.GREY)
            self.screen.blit(timestamp_text, (self.PHONE_X + 30, self.PHONE_Y + y_offset + notification_height - 25))
            y_offset += notification_height + 15
            if notification.startswith("WhatsApp"):
                self.whatsapp_popup_rect = notification_rect

        if self.all_notifications_shown:
            bottom_box_surface = pygame.Surface((self.WINDOW_WIDTH, 100), pygame.SRCALPHA)
            bottom_box_surface.fill((0, 0, 0, 0))
            self.screen.blit(bottom_box_surface, (0, self.WINDOW_HEIGHT - 100))
            bottom_message = "Click on WhatsApp"
            bottom_message_text = self.font_extra_large.render(bottom_message, True, self.WHITE)
            self.screen.blit(bottom_message_text, (self.WINDOW_WIDTH // 2 - bottom_message_text.get_width() // 2, self.WINDOW_HEIGHT - 90))

    def update_notifications(self):
        elapsed_time = time.time() - self.notification_start_time
        if elapsed_time > 2:
            if len(self.notifications) < 2:
                if len(self.notifications) == 0:
                    timestamp = "1 hour ago"
                    self.notifications.append(("News: THERE IS A BLACKOUT", timestamp, self.RED))
                elif len(self.notifications) == 1:
                    timestamp = "just now"
                    self.notifications.append(("WhatsApp: Unknown Sender", timestamp, self.GREEN))
                if len(self.notifications) == 2:
                    self.all_notifications_shown = True
            self.notification_start_time = time.time()

    def handle_click(self, position):
        if self.whatsapp_popup_rect.collidepoint(position):
            self.overlay_visible = True
            self.bottom_message_visible = True
            self.phone_visible = False
            self.transition_alpha = 255
            self.phone = WhatsAppPhone(self.screen)
            self.phone.update()

    def draw_overlay(self):
        if self.overlay_visible:
            overlay_surface = pygame.Surface((self.WINDOW_WIDTH, self.WINDOW_HEIGHT), pygame.SRCALPHA)
            overlay_surface.fill((0, 0, 0, 150))
            message_text = self.font_message.render("Clicked on WhatsApp", True, self.WHITE)
            overlay_surface.blit(message_text, (self.WINDOW_WIDTH // 2 - message_text.get_width() // 2, self.WINDOW_HEIGHT // 2 - message_text.get_height() // 2))
            #self.screen.blit(overlay_surface, (0, 0))

    def run(self):
        clock = pygame.time.Clock()
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.handle_click(event.pos)
                        return

            self.update_notifications()

            self.screen.blit(self.background_img, (0, 0))
            if self.phone_visible:
                self.draw_phone_frame()
                self.draw_lock_screen()
            else:
                if self.transition_alpha > 0:
                    self.transition_alpha -= 5

            #self.draw_overlay()
            pygame.display.flip()
            clock.tick(60)
        
        
'''pygame.init()
phone_lock_screen = PhoneLockScreen()
phone_lock_screen.run()'''

