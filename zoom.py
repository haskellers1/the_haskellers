import pygame
from pygameZoom import PygameZoom

class Window:
    def __init__(self):
        pygame.init()
        self.WIDTH, self.HEIGHT = 1200, 800
        self.WIN = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
        self.CLOCK = pygame.time.Clock()
        self.FPS = 15
        self.switch_image_after_zoom = False
        self.run = True
        self.pygameZoom = PygameZoom(self.WIDTH, self.HEIGHT)
        self.pygameZoom.set_background((0, 0, 0))  # Set background to black (optional)
        self.scale_factor = 1.0  # Initial scale factor (1.0 is original size)
        self.target_scale = 2.0  # Target scale factor (twice the original size)
        self.scale_increment = 0.01  # Increment value for scaling
        self.loop()

    def loop(self):
        while self.run:
            self.refresh_window()
            self.events()
            self.CLOCK.tick(self.FPS)
        pygame.quit()

    def events(self):
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                pygame.quit()

    def refresh_window(self):
        self.WIN.fill(0)  # Fill window with black (optional)
        
        # Load the image
        image = pygame.image.load("passage_hk.jpg")
        original_width, original_height = image.get_width(), image.get_height()
        
        # Gradually increase scale factor until target scale is reached
        if self.scale_factor < self.target_scale:
            self.scale_factor += self.scale_increment
        
        # Calculate new dimensions based on scale factor
        new_width = int(original_width * self.scale_factor)
        new_height = int(original_height * self.scale_factor)
        
        # Calculate position to center the scaled image
        x = (self.WIDTH - new_width) // 2
        y = (self.HEIGHT - new_height) // 2
        
        # Scale the image
        scaled_image = pygame.transform.scale(image, (new_width, new_height))
        
        # Clear the window and draw the scaled image
        self.WIN.blit(scaled_image, (x, y))
        
        pygame.display.update()

    def zoom_end(self):
        return self.scale_factor >= self.target_scale

# Initialize pygame and start the window
pygame.init()
ZOOMM = Window()
pygame.quit()
