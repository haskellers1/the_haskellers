import pygame
import sys
import time


class button:
    def __init__(self, text, width, height, pos, elevation, gui_font):
        self.pressed = False
        self.elevation = elevation
        self.dynamic_elevation = elevation
        self.original_y_pos = pos[1]

        self.top_rect = pygame.Rect(pos, (width, height))
        self.top_color = '#475F77'

        self.bottom_rect = pygame.Rect(pos, (width, height))
        self.bottom_color = '#354B5E'

        self.text_surf = gui_font.render(text, True, '#FFFFFF')
        self.text_rect = self.text_surf.get_rect(center=self.top_rect.center)

    def draw(self, screen):
        self.top_rect.y = self.original_y_pos - self.dynamic_elevation
        self.text_rect.center = self.top_rect.center

        self.bottom_rect.midtop = self.top_rect.midtop
        self.bottom_rect.height = self.top_rect.height + self.dynamic_elevation

        pygame.draw.rect(screen, self.bottom_color, self.bottom_rect, border_radius=12)
        pygame.draw.rect(screen, self.top_color, self.top_rect, border_radius=12)
        screen.blit(self.text_surf, self.text_rect)
        return self.check_click()

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.top_rect.collidepoint(mouse_pos):
            self.top_color = '#D74B4B'
            if pygame.mouse.get_pressed()[0]:
                self.dynamic_elevation = 0
                self.pressed = True
            else:
                self.dynamic_elevation = self.elevation
                if self.pressed:
                    self.pressed = False
                    return True
        else:
            self.dynamic_elevation = self.elevation
            self.top_color = '#475F77'
        return False


class BackgroundTransition:
    def __init__(self):

        pygame.init()

        self.WIDTH, self.HEIGHT = 1200, 800
        self.BUTTON_POSITION = (self.WIDTH // 2 - 100, self.HEIGHT // 2 + 200)
        self.BUTTON_TEXT = "Start"
        self.FONT_SIZE = 50
        self.image_display_duration = 2
        self.black_screen_duration = 2
        self.fade_duration = 1
        self.alpha_step = 255 // (self.fade_duration * 10)

        self.screen = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
        self.font = pygame.font.SysFont("Inkfree", self.FONT_SIZE)

        self.button = button(self.BUTTON_TEXT, 200, 40, self.BUTTON_POSITION, 5, self.font)

        self.background_images = [
            pygame.transform.scale(pygame.image.load('haunted_house.png').convert(), (self.WIDTH, self.HEIGHT)),
            pygame.transform.scale(pygame.image.load('inside_house.jpg').convert(), (self.WIDTH, self.HEIGHT)),
            pygame.transform.scale(pygame.image.load('room2.jpg').convert(), (self.WIDTH, self.HEIGHT))
        ]

        self.black_screen = pygame.Surface((self.WIDTH, self.HEIGHT))
        self.black_screen.fill((0, 0, 0))
        self.current_image_index = 0
        self.show_black_screen = False
        self.last_switch_time = time.time()
        self.background_count = 0
        self.start_clicked = False
        self.running = True

    def fade_in(self, image):
        overlay = pygame.Surface((self.WIDTH, self.HEIGHT))
        for alpha in range(0, 256, self.alpha_step):
            overlay.set_alpha(255 - alpha)
            self.screen.blit(image, (0, 0))
            self.screen.blit(overlay, (0, 0))
            pygame.display.flip()
            pygame.time.delay(int(self.fade_duration * 1000 / (255 / self.alpha_step)))

    def fade_out(self, image):
        overlay = pygame.Surface((self.WIDTH, self.HEIGHT))
        for alpha in range(0, 256, self.alpha_step):
            overlay.set_alpha(alpha)
            self.screen.blit(image, (0, 0))
            self.screen.blit(overlay, (0, 0))
            pygame.display.flip()
            pygame.time.delay(int(self.fade_duration * 1000 / (255 / self.alpha_step)))

    def handle_start_screen(self):
        while not self.start_clicked:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if self.button.top_rect.collidepoint(event.pos):
                        self.start_clicked = True

            self.screen.blit(self.background_images[0], (0, 0))
            self.button.draw(self.screen)
            pygame.display.flip()

    def handle_background_transitions(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            current_time = time.time()

            if self.show_black_screen and current_time - self.last_switch_time >= self.black_screen_duration:
                self.show_black_screen = False
                self.last_switch_time = current_time
                self.current_image_index = (self.current_image_index + 1) % len(self.background_images)
                self.background_count += 1
                self.fade_in(self.background_images[self.current_image_index])
            elif not self.show_black_screen and current_time - self.last_switch_time >= self.image_display_duration:
                self.show_black_screen = True
                self.last_switch_time = current_time
                self.fade_out(self.background_images[self.current_image_index])

            if self.show_black_screen:
                self.screen.blit(self.black_screen, (0, 0))
            else:
                self.screen.blit(self.background_images[self.current_image_index], (0, 0))

            pygame.display.flip()
            pygame.time.delay(100)

            if self.background_count >= 2 and self.show_black_screen:
                self.running = False

        pygame.quit()

    def run(self):
        self.handle_start_screen()
        self.handle_background_transitions()
        flashcards_game = FlashcardsGame()
        flashcards_game.run()


class FlashcardsGame:
    def __init__(self):
        self.flashcard_sets = [
            ["Char", "Double", "Boolean"],
            ["10", "14", "15"],
            [
                "PyHaskell",
                "Glasgow Haskell Compiler",
                "CHaskell"
            ]
        ]
        self.question_sets = [
            "Which of the following is not a built-in data type in Haskell?",
            "What will be the output when fac is called with an argument of 5:\n"
            "fac :: Integer -> Integer\n"
            "fac n | n == 0 = 0\n"
            "fac n | n > 0 = n + fac(n-1)",
            "What is the main implementation of Haskell?"
        ]

        self.current_set = 0
        self.sets_shown = 0
        self.flipped = [False, False, False]
        self.auto_flip_timer = None
        self.green_flipped = False
        self.clicked = [False, False, False]

        self.total_width = 3 * FLASHCARD_WIDTH + 2 * SPACING
        self.start_x = (WIDTH - self.total_width) // 2
        if self.start_x < 0:
            self.start_x = 0

        self.next_button = Button('Next', 200, 50, (WIDTH // 2 - 100, HEIGHT - 100), 5, gui_font)

    def draw_flashcard(self, surface, x, y, color, depth):
        pygame.draw.rect(surface, SHADOW_COLOR, (x + SHADOW_OFFSET, y + SHADOW_OFFSET, FLASHCARD_WIDTH, FLASHCARD_HEIGHT))
        for i in range(1, depth):
            pygame.draw.line(surface, SHADOW_COLOR, (x + i + SHADOW_OFFSET, y + SHADOW_OFFSET),
                             (x + i + SHADOW_OFFSET, y + FLASHCARD_HEIGHT + SHADOW_OFFSET))
            pygame.draw.line(surface, SHADOW_COLOR, (x + SHADOW_OFFSET, y + i + SHADOW_OFFSET),
                             (x + FLASHCARD_WIDTH + SHADOW_OFFSET, y + i + SHADOW_OFFSET))

        pygame.draw.rect(surface, color, (x, y, FLASHCARD_WIDTH, FLASHCARD_HEIGHT))

    def fade(self, screen, color, duration=1000):
        fade_surface = pygame.Surface((WIDTH, HEIGHT))
        fade_surface.fill(color)
        for alpha in range(0, 255):
            fade_surface.set_alpha(alpha)
            screen.blit(fade_surface, (0, 0))
            pygame.display.flip()
            pygame.time.delay(duration // 255)

    def next_flashcards(self):
        self.flipped = [False, False, False]
        self.clicked = [False, False, False]  # Reset clicked status for new screen
        self.current_set += 1
        self.sets_shown += 1
        self.auto_flip_timer = None
        self.green_flipped = False

    def run(self):
        while self.sets_shown < len(self.flashcard_sets):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    mouse_x, mouse_y = pygame.mouse.get_pos()
                    if (not self.flipped[0] and self.start_x <= mouse_x <= self.start_x + FLASHCARD_WIDTH and
                            (HEIGHT // 2) <= mouse_y <= (HEIGHT // 2) + FLASHCARD_HEIGHT):
                        self.flipped[0] = True
                    elif (not self.flipped[1] and self.start_x + FLASHCARD_WIDTH + SPACING <= mouse_x <= self.start_x + 2 * FLASHCARD_WIDTH + SPACING and
                          (HEIGHT // 2) <= mouse_y <= (HEIGHT // 2) + FLASHCARD_HEIGHT):
                        self.flipped[1] = True
                    elif (not self.flipped[2] and self.start_x + 2 * FLASHCARD_WIDTH + 2 * SPACING <= mouse_x <= self.start_x + 3 * FLASHCARD_WIDTH + 2 * SPACING and
                          (HEIGHT // 2) <= mouse_y <= (HEIGHT // 2) + FLASHCARD_HEIGHT):
                        self.flipped[2] = True

            screen.fill(BACKGROUND_COLOR)
            text_surface = question_font.render(self.question_sets[self.current_set], True, (0, 0, 0))
            text_rect = text_surface.get_rect(center=(WIDTH // 2, HEIGHT // 2 - FLASHCARD_HEIGHT))
            screen.blit(text_surface, text_rect)

            for i, color in enumerate(FLASHCARD_COLORS):
                x = self.start_x + i * (FLASHCARD_WIDTH + SPACING)
                self.draw_flashcard(screen, x, HEIGHT // 2, color, 15)
                if self.flipped[i]:
                    answer_text = answer_font.render(self.flashcard_sets[self.current_set][i], True, (0, 0, 0))
                    answer_rect = answer_text.get_rect(center=(x + FLASHCARD_WIDTH // 2, HEIGHT // 2 + FLASHCARD_HEIGHT // 2))
                    screen.blit(answer_text, answer_rect)

            if all(self.flipped):
                if not self.auto_flip_timer:
                    self.auto_flip_timer = pygame.time.get_ticks()
                elif pygame.time.get_ticks() - self.auto_flip_timer >= AUTO_FLIP_DELAY:
                    self.next_flashcards()

            pygame.display.flip()
            pygame.time.Clock().tick(60)

        self.fade(screen, BACKGROUND_COLOR)
        pygame.time.wait(2000)
        pygame.quit()
        sys.exit()


if __name__ == "__main__":
    transition_game = BackgroundTransition()
    transition_game.run()


