import pygame
import sys
import random


class Confetti:
    def __init__(self, screen, num_particles=100):
        self.screen = screen
        self.num_particles = num_particles
        self.particles = []
        self.create_particles()

    def create_particles(self):
        for _ in range(self.num_particles):
            x = random.randint(0, Popup.SCREEN_WIDTH)
            y = random.randint(-20, Popup.SCREEN_HEIGHT)
            speed_y = random.uniform(1, 3)
            size = random.randint(2, 5)
            color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            self.particles.append([x, y, speed_y, size, color])

    def update(self):
        for particle in self.particles:
            particle[1] += particle[2]
            if particle[1] > Popup.SCREEN_HEIGHT:
                particle[1] = random.randint(-20, -1)
                particle[0] = random.randint(0, Popup.SCREEN_WIDTH)

    def draw(self):
        for particle in self.particles:
            pygame.draw.circle(self.screen, particle[4], (int(particle[0]), int(particle[1])), particle[3])

class Popup:
    SCREEN_WIDTH = 1200
    SCREEN_HEIGHT = 800
    POPUP_WIDTH = 400
    POPUP_HEIGHT = 200
    DIM_COLOR = (0, 0, 0, 128)  
    POPUP_DELAY = 2000  
    FADE_IN_DURATION = 1000  
    BORDER_RADIUS = 20  
    CIRCLE_RADIUS = 30  
    CIRCLE_COLOR = (255, 215, 0)

    # Colors
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)

    def __init__(self, screen, text):
        self.text = text
        self.screen = screen
        pygame.display.set_caption('Popup Box Example')
        self.clock = pygame.time.Clock()
        self.start_time = pygame.time.get_ticks()
        self.rect = pygame.Rect((self.SCREEN_WIDTH // 2 - self.POPUP_WIDTH // 2,
                                 self.SCREEN_HEIGHT // 2 - self.POPUP_HEIGHT // 2,
                                 self.POPUP_WIDTH, self.POPUP_HEIGHT))
        self.surface = pygame.Surface((self.POPUP_WIDTH, self.POPUP_HEIGHT), pygame.SRCALPHA)
        self.surface.set_alpha(0)
        self.alpha = 0

        self.font = pygame.font.Font(None, 36)
        self.render_text()

        self.confetti = Confetti(self.screen)
        self.random_number = random.randint(0, 9)

    def render_text(self):
        self.text_surface = self.font.render(self.text, True, self.BLACK)
        self.text_rect = self.text_surface.get_rect(center=(self.POPUP_WIDTH // 2, self.POPUP_HEIGHT // 2 - self.CIRCLE_RADIUS - 20))

    def draw_rounded_rect(self):
        rounded_rect_surface = pygame.Surface((self.POPUP_WIDTH, self.POPUP_HEIGHT), pygame.SRCALPHA)
        pygame.draw.rect(rounded_rect_surface, self.WHITE, rounded_rect_surface.get_rect(), border_radius=self.BORDER_RADIUS)
        return rounded_rect_surface

    def draw_number_circle(self):
        circle_center = (self.POPUP_WIDTH // 2, self.POPUP_HEIGHT // 2 + self.CIRCLE_RADIUS // 2)
        pygame.draw.circle(self.surface, self.CIRCLE_COLOR, circle_center, self.CIRCLE_RADIUS)
        number_surface = self.font.render(str(self.random_number), True, self.BLACK)
        number_rect = number_surface.get_rect(center=circle_center)
        self.surface.blit(number_surface, number_rect)

    def run(self):
        run_duration = 5000
        start_time = pygame.time.get_ticks()
        while pygame.time.get_ticks() - start_time < run_duration:
            self.handle_events()
            self.update()
            self.draw()
        return self.random_number

    def handle_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

    def update(self):
        self.confetti.update()

    def draw(self):
        current_time = pygame.time.get_ticks()
        elapsed_time = current_time - self.start_time

        self.screen.fill(self.BLACK)
        
        if elapsed_time > self.POPUP_DELAY:
            self.update_alpha(elapsed_time - self.POPUP_DELAY)
            self.draw_dim_background()
            rounded_surface = self.draw_rounded_rect()
            rounded_surface.set_alpha(self.alpha)
            self.screen.blit(rounded_surface, self.rect.topleft)
            self.surface.blit(self.text_surface, self.text_rect)
            self.draw_number_circle()
            self.screen.blit(self.surface, self.rect.topleft)
            self.confetti.draw()

        pygame.display.flip()
        self.clock.tick(30)

    def update_alpha(self, elapsed_time):
        if elapsed_time < self.FADE_IN_DURATION:
            self.alpha = int((elapsed_time / self.FADE_IN_DURATION) * 255)
        else:
            self.alpha = 255
        
        self.surface.set_alpha(self.alpha)

    def draw_dim_background(self):
        dim_surface = pygame.Surface((self.SCREEN_WIDTH, self.SCREEN_HEIGHT), pygame.SRCALPHA)
        dim_surface.fill(self.DIM_COLOR)
        self.screen.blit(dim_surface, (0, 0))


'''pygame.init()
screen = pygame.display.set_mode((1200, 800))
popup = Popup(screen, "Your digit is here!")
num = popup.run()
print(num)'''
   
