import pygame
import time
from button import Button

# Initialize Pygame
pygame.init()

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800
QUESTION_POSITION = (40, 40)
BUTTON_POSITION = (900, 700)
BUTTON_TEXT = "Next"
FONT_SIZE = 50
LIVES = 3

# Initialize pygame window
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Find the Error")
font = pygame.font.SysFont("Inkfree", FONT_SIZE)
next_button = Button(BUTTON_TEXT, 200, 40, BUTTON_POSITION, 5, font)

# Quiz data
codes = [
    "next :: Int -> Int\nnext n for (rem n 2) == 0 then (div n 2) else (3 * n + 1)\n\ncollatz :: Int -> [Int]\ncollatz 4 = [4, 2, 1]\ncollatz n = [n] ++ (collatz $ next n)\n\nmain = print $ collatz 7",
    "primes = filterPrime [2..] WHERE\n  filterPrime (p:xs) =\n  p : filterPrime [x | x <- xs, x `mod` p /= 0]",
    "import Data.List \n\nnumToDigits :: Int -> [Int]\nnumToDigits = map digitToInt . show"
]
answers = ["for", "WHERE", "Data.List"]
qcount = 0


# Function to display text and handle user interaction
def display_text_and_get_answer(text, pos, font, color):
    words_surf = []
    words_pos = []
    words_text = []

    collection = [word.split(' ') for word in text.splitlines()]
    space = font.size(' ')[0]
    x, y = pos

    for lines in collection:
        for words in lines:
            word_surface = font.render(words, True, color)
            word_width, word_height = word_surface.get_size()

            if x + word_width >= SCREEN_WIDTH - 40:
                x = pos[0]
                y += word_height

            words_pos.append((x, y))
            words_surf.append(word_surface)
            words_text.append(words)
            x += word_width + space

        x = pos[0]
        y += word_height

    return words_text, words_pos, words_surf


def get_question(text):
    words_text, words_pos, words_surf = display_text_and_get_answer(text, QUESTION_POSITION, font,
                                                                    pygame.Color('white'))
    return words_text, words_pos, words_surf


def draw_rounded_button(surface, color, rect, radius, text, font):
    """Draw a rounded rectangle button with text."""
    pygame.draw.rect(surface, color, rect, border_radius=radius)
    text_surface = font.render(text, True, pygame.Color('black'))
    text_rect = text_surface.get_rect(center=rect.center)
    surface.blit(text_surface, text_rect)


def text_up_down(text, y_position, font, screen):
    """Animate text moving up and down."""
    input_surface = font.render(text, True, pygame.Color('white'))
    for offset in range(-10, 10, 5):  # Move text up and down
        screen.fill(pygame.Color('black'))
        button_rect = pygame.Rect(400, 600, 400, 60)
        draw_rounded_button(screen, pygame.Color('lightgrey'), button_rect, 30, 'Restart Level', font)
        screen.blit(input_surface, (250, y_position + offset))
        pygame.display.flip()  # Use flip for double buffering
        pygame.time.delay(150)  # Delay for animation effect


def display_you_lose(font, screen):
    """Display the 'You Lose' page."""
    screen.fill(pygame.Color('black'))  # Fill the screen with black

    # Draw the button
    button_rect = pygame.Rect(400, 600, 400, 60)
    draw_rounded_button(screen, pygame.Color('lightgrey'), button_rect, 30, 'Restart Level', font)

    # Display "You Lose" text with animation
    text_up_down('You Lose', 250, font, screen)  # Adjusted y_position to move text up

    # Draw the button again to ensure it's on top
    draw_rounded_button(screen, pygame.Color('lightgrey'), button_rect, 30, 'Restart Level', font)

    pygame.display.update()


def error_game(text, ans, font, screen):
    words, position, surface = get_question(text)
    lives = LIVES
    while True:
        pygame.display.update()
        screen.fill(pygame.Color('black'))

        # Display lives
        lives_text = f"Lives: {lives}"
        lives_surface = font.render(lives_text, True, pygame.Color('white'))
        screen.blit(lives_surface, (10, 10))

        answered_correctly = False  # Reset for each guess

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if not answered_correctly:
                    for i in range(len(surface)):
                        button_rect = surface[i].get_rect(topleft=position[i])
                        if button_rect.collidepoint(event.pos):
                            if words[i] == ans:
                                surface[i] = font.render(words[i], True, pygame.Color('green'))
                                answered_correctly = True
                                break
                            else:
                                surface[i] = font.render(words[i], True, pygame.Color('red'))
                                lives -= 1
                                if lives <= 0:
                                    display_you_lose(font, screen)
                                    pygame.time.delay(2000)
                                    return False
                                break
                if next_button.top_rect.collidepoint(event.pos):
                    return True

        for i in range(len(words)):
            screen.blit(surface[i], position[i])

        if answered_correctly:
            next_button.draw(screen)


def play_game(qcount):
    while qcount < len(codes):
        correct = error_game(codes[qcount], answers[qcount], font, screen)
        if correct:
            qcount += 1


play_game(qcount)
pygame.quit()

