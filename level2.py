import cv2
import pygame
from L2start import Game


class VideoPlayer:
    def __init__(self, video_file):
        self.video_file = video_file
        self.cap = cv2.VideoCapture(self.video_file)
        if not self.cap.isOpened():
            print(f'Error opening video file {self.video_file}')
            pygame.quit()
            return
        
        self.frame_width = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.frame_height = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        pygame.display.set_caption('Video Player')
        self.clock = pygame.time.Clock()
        self.playing = True

    def play(self, screen):
        while self.playing:
            ret, frame = self.cap.read()
            if not ret:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = pygame.image.frombuffer(frame.tobytes(), (self.frame_width, self.frame_height), 'RGB')

            
            screen.blit(frame, (0, 0))
            pygame.display.flip()
            self.clock.tick(30)
        
        self.cap.release()

    def stop(self):
        self.playing = False


'''pygame.init()
screen = pygame.display.set_mode((1200, 800))
video_player = VideoPlayer('passage.mp4')
video_player.play(screen)
game = Game(screen)
game.run()'''
