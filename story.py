import pygame
from fill import Fill_Game

pygame.init()

clock = pygame.time.Clock()
FPS = 60

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("TriHask")

scroll = 0

ground_image = pygame.image.load("fg.png").convert_alpha()
ground_image = pygame.transform.scale(ground_image, (SCREEN_WIDTH, SCREEN_HEIGHT // 4))
ground_width = ground_image.get_width()
ground_height = ground_image.get_height()

bg_images = []
for i in range(1, 5):
    bg_image = pygame.image.load(f"bg-{i}.png").convert_alpha()
    bg_image = pygame.transform.scale(bg_image, (SCREEN_WIDTH, SCREEN_HEIGHT))
    bg_images.append(bg_image)
bg_width = bg_images[0].get_width()

player_image = pygame.image.load("player.png").convert_alpha()
player_image = pygame.transform.scale(player_image, (80, 100))

interaction_image = pygame.image.load("key_game.png").convert_alpha() 
interaction_image = pygame.transform.scale(interaction_image, (100, 100))

#pygame.mixer.music.load("bg-music.mp3")
#pygame.mixer.music.play(-1)


def draw_bg():
    for x in range(5):
        speed = 1
        for idx, bg_image in enumerate(bg_images):
            y_pos = 0
            if idx == 3:
                y_pos = SCREEN_HEIGHT - bg_image.get_height() - 50
            elif idx == 0:
                y_pos = SCREEN_HEIGHT - bg_image.get_height()
            screen.blit(bg_image, ((x * bg_width) - scroll * speed, y_pos))
            speed += 0.2


def draw_ground():
    for x in range(10):
        screen.blit(ground_image,
                    ((x * ground_width) - scroll * 2.5, SCREEN_HEIGHT - ground_height + 10)) 


bg_width, bg_height = bg_image.get_rect().size

stage_width = bg_width * 2
stage_pos_x = 0
start_scrolling_pos_x = SCREEN_WIDTH / 2

player_width, player_height = player_image.get_rect().size
player_pos_x = player_width // 2
player_pos_y = SCREEN_HEIGHT - ground_height - player_height + 180
player_velocity_x = 0

interaction_x = player_pos_x + 100
interaction_y = player_pos_y

font = pygame.font.SysFont(None, 48)
popup_visible = False


def draw_popup():
    pygame.draw.rect(screen, (255, 255, 255),
                     (SCREEN_WIDTH // 4, SCREEN_HEIGHT // 4, SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2))

    
    shadow_rect = pygame.Rect(SCREEN_WIDTH // 4 + 4, SCREEN_HEIGHT // 4 + 4, SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2)
    pygame.draw.rect(screen, (50, 50, 50), shadow_rect)

    button_rect = pygame.Rect(SCREEN_WIDTH // 2 - 100, SCREEN_HEIGHT // 2 - 25, 200, 50)
    pygame.draw.rect(screen, (0, 0, 0), button_rect)

    text = font.render("Hack", True, (255, 255, 255))
    text_rect = text.get_rect(center=button_rect.center)
    screen.blit(text, text_rect)

    return button_rect

run = True
while run:
    clock.tick(FPS)

    draw_bg()
    draw_ground()

    key = pygame.key.get_pressed()
    if key[pygame.K_LEFT] and player_pos_x > 0:
        player_velocity_x = -5
    elif key[pygame.K_RIGHT] and player_pos_x < stage_width - player_width:
        player_velocity_x = 5
    else:
        player_velocity_x = 0

    player_pos_x += player_velocity_x

    if player_pos_x > stage_width - player_width // 2:
        player_pos_x = stage_width - player_width // 2
    if player_pos_x < player_width // 2:
        player_pos_x = player_width // 2

    if player_pos_x < start_scrolling_pos_x:
        scroll = 0
    elif player_pos_x > stage_width - start_scrolling_pos_x:
        scroll = stage_width - SCREEN_WIDTH
    else:
        scroll = player_pos_x - start_scrolling_pos_x

    screen.blit(interaction_image, (interaction_x - scroll, interaction_y))

    screen.blit(player_image, (player_pos_x - scroll, player_pos_y))

    player_rect = pygame.Rect(player_pos_x - scroll, player_pos_y, player_width, player_height)
    interaction_rect = pygame.Rect(interaction_x - scroll, interaction_y, interaction_image.get_width(),
                                   interaction_image.get_height())

    if player_rect.colliderect(interaction_rect):
        if not popup_visible:  
            popup_visible = True
    else:
        popup_visible = False

    if popup_visible:
        button_rect = draw_popup()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  
                if popup_visible:
                    if button_rect.collidepoint(event.pos):
                      
                        game = Fill_Game()
                        game.play_game()
                        print("Hack button clicked!")
                    popup_visible = False

    pygame.display.update()

pygame.quit()

