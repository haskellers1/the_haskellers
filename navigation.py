import pygame
import sys
import subprocess
from rules import Rules
from levels import GameController

class Button:
    pygame.font.init()
    gui_font = pygame.font.Font(None, 30)

    def __init__(self, text, width, height, pos, elevation):
        self.pressed = False
        self.elevation = elevation
        self.dynamic_elevation = elevation
        self.original_y_pos = pos[1]
        self.text = text

        self.top_rect = pygame.Rect(pos, (width, height))
        self.top_color = '#475F77'

        self.bottom_rect = pygame.Rect(pos, (width, height))
        self.bottom_color = '#354B5E'

        self.update_text_surface()

    def update_text_surface(self):
        self.text_surf = self.gui_font.render(self.text, True, '#FFFFFF')
        self.text_rect = self.text_surf.get_rect(center=self.top_rect.center)

    def draw(self, screen):
        self.top_rect.y = self.original_y_pos - self.dynamic_elevation
        self.text_rect.center = self.top_rect.center

        self.bottom_rect.midtop = self.top_rect.midtop
        self.bottom_rect.height = self.top_rect.height + self.dynamic_elevation

        pygame.draw.rect(screen, self.bottom_color, self.bottom_rect, border_radius=12)
        pygame.draw.rect(screen, self.top_color, self.top_rect, border_radius=12)
        screen.blit(self.text_surf, self.text_rect)
        return self.check_click()

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.top_rect.collidepoint(mouse_pos):
            self.top_color = '#D74B4B'
            if pygame.mouse.get_pressed()[0]:
                self.dynamic_elevation = 0
                self.pressed = True
            else:
                self.dynamic_elevation = self.elevation
                if self.pressed:
                    self.pressed = False
                    return True
        else:
            self.dynamic_elevation = self.elevation
            self.top_color = '#475F77'
        return False


class SidebarApp:
    SCREEN_WIDTH = 1200
    SCREEN_HEIGHT = 800
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    GRAY = (200, 200, 200)
    RED = (255, 0, 0)
    BUTTON_GRAY = (169, 169, 169)
    DUCK_ICON_PATH = 'duck_icon.png'
    DUCK_SIZE = (70, 70)
    SIDEBAR_WIDTH = 200
    SIDEBAR_HEIGHT = SCREEN_HEIGHT
    CLOSE_BUTTON_SIZE = 30
    BUTTON_WIDTH = 160
    BUTTON_HEIGHT = 40
    BUTTON_MARGIN = 20
    BUTTON_RADIUS = 12

    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        pygame.display.set_caption("Sidebar")
        self.clock = pygame.time.Clock()
        self.duck_icon = pygame.image.load(self.DUCK_ICON_PATH)
        self.duck_icon = pygame.transform.scale(self.duck_icon, self.DUCK_SIZE)
        self.duck_width, self.duck_height = self.duck_icon.get_size()
        self.duck_x = self.SCREEN_WIDTH - self.duck_width - 10
        self.duck_y = 10

        self.sidebar_open = False
        self.sidebar_x = self.SCREEN_WIDTH

        self.buttons = [
            Button(name, self.BUTTON_WIDTH, self.BUTTON_HEIGHT, (self.sidebar_x + 20, 60 + 2 * (self.BUTTON_HEIGHT + self.BUTTON_MARGIN) + i * (self.BUTTON_HEIGHT + self.BUTTON_MARGIN)), 5)
            for i, name in enumerate(['Home', 'Rules', 'Quit'])
        ]

    def draw_sidebar(self):
        if self.sidebar_open:
            pygame.draw.rect(self.screen, self.GRAY, (self.sidebar_x, 0, self.SIDEBAR_WIDTH, self.SIDEBAR_HEIGHT))
            self.draw_close_button()
            self.draw_sidebar_buttons()

    def draw_close_button(self):
        close_button_x = self.sidebar_x + 10
        close_button_y = 10
        pygame.draw.rect(self.screen, self.RED, (close_button_x, close_button_y, self.CLOSE_BUTTON_SIZE, self.CLOSE_BUTTON_SIZE))
        pygame.draw.line(self.screen, self.WHITE, (close_button_x + 5, close_button_y + 5), (close_button_x + self.CLOSE_BUTTON_SIZE - 5, close_button_y + self.CLOSE_BUTTON_SIZE - 5), 2)
        pygame.draw.line(self.screen, self.WHITE, (close_button_x + self.CLOSE_BUTTON_SIZE - 5, close_button_y + 5), (close_button_x + 5, close_button_y + self.CLOSE_BUTTON_SIZE - 5), 2)

    def draw_sidebar_buttons(self):
        for button in self.buttons:
            if button.draw(self.screen):
                print(f"Button '{button.text}' pressed.")
                if button.text == 'Home':
                    game = GameController(self.screen)
                    game.run()
                    try:
                        subprocess.Popen(["python3", "homepage.py"])
                    except Exception as e:
                        print(f"Error executing 'homepage.py': {e}")
                elif button.text == 'Rules':
                    rules = Rules(self.screen)
                    rules.draw()
                    pygame.display.update()
                    pygame.time.delay(5000)
                    try:
                        subprocess.Popen(["python3", "rules.py"])
                    except Exception as e:
                        print(f"Error executing 'rules.py': {e}")
                elif button.text == 'Quit':
                    pygame.quit()
                    sys.exit()

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    mouse_x, mouse_y = pygame.mouse.get_pos()

                    if self.duck_x <= mouse_x <= self.duck_x + self.duck_width and self.duck_y <= mouse_y <= self.duck_y + self.duck_height:
                        self.sidebar_open = not self.sidebar_open
                        if self.sidebar_open:
                            self.sidebar_x = self.SCREEN_WIDTH - self.SIDEBAR_WIDTH
                            for i, button in enumerate(self.buttons):
                                button.top_rect.x = self.sidebar_x + 20
                                button.bottom_rect.x = self.sidebar_x + 20
                                button.top_rect.y = 60 + 2 * (self.BUTTON_HEIGHT + self.BUTTON_MARGIN) + i * (self.BUTTON_HEIGHT + self.BUTTON_MARGIN)
                                button.bottom_rect.y = 60 + 2 * (self.BUTTON_HEIGHT + self.BUTTON_MARGIN) + i * (self.BUTTON_HEIGHT + self.BUTTON_MARGIN)
                                button.update_text_surface()
                        else:
                            self.sidebar_x = self.SCREEN_WIDTH

                    close_button_x = self.sidebar_x + 10
                    close_button_y = 10
                    if self.sidebar_open and close_button_x <= mouse_x <= close_button_x + self.CLOSE_BUTTON_SIZE and close_button_y <= mouse_y <= close_button_y + self.CLOSE_BUTTON_SIZE:
                        self.sidebar_open = False
                        self.sidebar_x = self.SCREEN_WIDTH

                    if self.sidebar_open:
                        self.draw_sidebar_buttons()

            self.screen.fill(self.BLACK)
            self.screen.blit(self.duck_icon, (self.duck_x, self.duck_y))
            self.draw_sidebar()
            pygame.display.flip()
            self.clock.tick(60)

        pygame.quit()
        sys.exit()

if __name__ == "__main__":
    app = SidebarApp()
    app.run()

  
