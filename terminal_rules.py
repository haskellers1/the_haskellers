import pygame
import sys
from button import Button

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800

class Terminal_Rules:
    def __init__(self, screen):
        self.screen = screen
        self.background_image = pygame.image.load('rules.png')
        self.background_image = pygame.transform.scale(self.background_image, (SCREEN_WIDTH, SCREEN_HEIGHT))
        self.back_button = Button("Back", 170, 40, (900, 600), 5, pygame.font.Font(None, 30))
        # Define font and bullet points
        self.font = pygame.font.Font(None, 36)
        self.bullet_points = [
            "2 levels - with 4 questions in each",
            "You can mavigate to levels page using back button",
            "You have to rearrange the code lines",
            "You can also type code in terminal",
            ]
        self.text_color = (0, 0, 0)  # Black color
        self.start_x = 270
        self.start_y = 170
        self.line_spacing = 65

    def draw(self):
        self.screen.blit(self.background_image, (0, 0))
        self.render_bullet_points()


    def render_bullet_points(self):
        y = self.start_y
        for point in self.bullet_points:
            text_surface = self.font.render(f"• {point}", True, self.text_color)
            self.screen.blit(text_surface, (self.start_x, y))
            y += text_surface.get_height() + self.line_spacing


