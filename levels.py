from homepage import Homepage
#from navigation import SidebarAPP
from L2start import Game
from level2 import VideoPlayer
from story3 import TriHaskGame
from flashcards import FlashcardsGame
from story2 import BackgroundTransition
from popup import Popup
from end import Ending
from start import Start

class GameController:
    def __init__(self, screen):
        self.screen = screen
        #self.sidebar = SidebarApp(screen)
        self.pin = ""

    def run(self):
        self.homepage()
        self.starting()
        self.background_transition()
        self.level1()
        self.video_player()
        self.level2()
        self.level3()
        self.end_screen()

    def homepage(self):
        homepage = Homepage(self.screen)
        homepage.run()

    def starting(self):
        starting = Start(self.screen)
        starting.update()

    def background_transition(self):
        game = BackgroundTransition(self.screen)
        game.run()

    def level1(self):
        game1 = FlashcardsGame(self.screen)
        game1.run()
        num1 = self.popup("Your digit is here!")
        self.pin += str(num1)

    def video_player(self):
        video_player = VideoPlayer('passage.mp4')
        video_player.play(self.screen)

    def level2(self):
        game2 = Game(self.screen)
        game2.run()
        num2 = self.popup("Your digit is here!")
        self.pin += str(num2)

    def level3(self):
        game3 = TriHaskGame(self.screen)
        game3.run()
        num3 = self.popup("Your digit is here!")
        self.pin += str(num3)
    

    def popup(self, message):
        popup = Popup(self.screen, message)
        return popup.run()

    def end_screen(self):
        endscreen = Ending(self.screen, self.pin)
        endscreen.run()

