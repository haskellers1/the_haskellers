import pygame
import sys
import datetime

class WhatsAppPhone:
    def __init__(self, screen):
        self.SCREEN_WIDTH = 1200
        self.SCREEN_HEIGHT = 800
        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)
        self.LIGHT_GRAY = (230, 230, 230)
        self.GREEN = (0, 200, 0)
        self.RED = (200, 0, 0)
        self.GREY = (200, 200, 200)
        self.PHONE_WIDTH = 400
        self.PHONE_HEIGHT = 800
        self.PHONE_X = (self.SCREEN_WIDTH - self.PHONE_WIDTH) // 2
        self.PHONE_Y = (self.SCREEN_HEIGHT - self.PHONE_HEIGHT) // 2
        self.screen = screen
        #self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        pygame.display.set_caption("TriHask")

        self.background_image = pygame.image.load('initial_room2.jpg')
        self.background_image = pygame.transform.scale(self.background_image, (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        self.profile_picture = pygame.image.load('hoodie.png')
        self.profile_picture = pygame.transform.scale(self.profile_picture, (70, 90))
        self.messages = [
            "No need to know who I am.",
            "Stop the blackout!",
            "Go to location.",
            "Hurry!!"
        ]
        pygame.font.init()
        self.font = pygame.font.SysFont('Arial', 20)
        self.timestamp_font = pygame.font.SysFont('Arial', 14)
        self.line_height = 50
        self.bubble_padding = 10
        self.message_interval = 2000
        self.status_interval = 5000
        self.message_start_time = pygame.time.get_ticks()
        self.status_start_time = pygame.time.get_ticks()
        self.status_online = True
        self.all_messages_displayed = False
        self.message_index = 0
        self.running = True
        self.fading = False
        self.fade_alpha = 255

    def draw_phone(self):
        self.screen.blit(self.background_image, (0, 0))
        pygame.draw.rect(self.screen, self.GREY, (self.PHONE_X - 10, self.PHONE_Y - 10, self.PHONE_WIDTH + 20, self.PHONE_HEIGHT + 20), border_radius=30)  
        pygame.draw.rect(self.screen, self.BLACK, (self.PHONE_X, self.PHONE_Y, self.PHONE_WIDTH, self.PHONE_HEIGHT), border_radius=20)  

        pygame.draw.rect(self.screen, self.WHITE, (self.PHONE_X, self.PHONE_Y, self.PHONE_WIDTH, 60), border_top_left_radius=20, border_top_right_radius=20)

        self.screen.blit(self.profile_picture, (self.PHONE_X + 10, self.PHONE_Y + 5))
        name_surface = self.font.render("Unknown", True, self.BLACK)
        self.screen.blit(name_surface, (self.PHONE_X + 70, self.PHONE_Y + 10))

        if self.all_messages_displayed:
            status_text = "Offline"
            status_color = self.RED
        else:
            status_text = "Online"
            status_color = self.GREEN
        status_surface = self.font.render(status_text, True, status_color)
        self.screen.blit(status_surface, (self.PHONE_X + 70, self.PHONE_Y + 30))

        wallpaper_image = pygame.image.load('wallpaper.jpg')
        wallpaper_image = pygame.transform.scale(wallpaper_image, (self.PHONE_WIDTH, self.PHONE_HEIGHT - 60))
        self.screen.blit(wallpaper_image, (self.PHONE_X, self.PHONE_Y + 60))

        pygame.draw.rect(self.screen, self.BLACK, (self.PHONE_X, self.PHONE_Y + 60, self.PHONE_WIDTH, self.PHONE_HEIGHT - 60), border_bottom_left_radius=20, border_bottom_right_radius=20, width=2)

        pygame.draw.rect(self.screen, self.GREY, (self.PHONE_X + 150, self.PHONE_Y + 10, 100, 10))  

    def render_messages(self):
        current_time = pygame.time.get_ticks()
        for i in range(self.message_index):
            if i < len(self.messages):
                message = self.messages[i]
                timestamp = datetime.datetime.now().strftime("%I:%M %p")

                message_surface = self.font.render(message, True, self.BLACK)
                timestamp_surface = self.timestamp_font.render(timestamp, True, self.BLACK)
                bubble_width = max(message_surface.get_width(), timestamp_surface.get_width()) + 2 * self.bubble_padding
                bubble_height = message_surface.get_height() + timestamp_surface.get_height() + 3 * self.bubble_padding

                bubble_x = self.PHONE_X + 70
                bubble_y = self.PHONE_Y + 80 + i * self.line_height * 1.5

                self.screen.blit(self.profile_picture, (self.PHONE_X + 20, bubble_y))
                pygame.draw.rect(self.screen, self.GREEN, (bubble_x, bubble_y, bubble_width, bubble_height), border_radius=15)

                self.screen.blit(message_surface, (bubble_x + self.bubble_padding, bubble_y + self.bubble_padding))

                self.screen.blit(timestamp_surface, (bubble_x + self.bubble_padding, bubble_y + bubble_height - timestamp_surface.get_height() - self.bubble_padding))

    def fade_out(self):
        fade_surface = pygame.Surface((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        fade_surface.fill(self.BLACK)
        fade_surface.set_alpha(self.fade_alpha)
        self.screen.blit(fade_surface, (0, 0))
        self.fade_alpha -= 5
        if self.fade_alpha <= 0:
            self.running = False
            from start_next import after_start
            start = after_start(self.screen)
            start.run()
            return

    def update(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            self.draw_phone()
            self.render_messages()

            if self.all_messages_displayed:
                self.fading = True

            if self.fading:
                self.fade_out()

            pygame.display.flip()

            current_time = pygame.time.get_ticks()
            if current_time - self.message_start_time > self.message_interval and self.message_index < len(self.messages):
                self.message_start_time = current_time
                self.message_index += 1

            if self.message_index >= len(self.messages):
                self.all_messages_displayed = True

            if current_time - self.status_start_time > self.status_interval and not self.all_messages_displayed:
                self.status_start_time = current_time
                self.status_online = not self.status_online


'''pygame.init()
phone = WhatsAppPhone()
phone.update()
'''
