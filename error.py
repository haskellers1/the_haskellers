import pygame
import sys
from button import Button

pygame.init()

class Game:
    SCREEN_WIDTH = 1200
    SCREEN_HEIGHT = 800
    QUESTION_POSITION = (40, 40)
    BUTTON_POSITION = (900, 700)
    BUTTON_TEXT = "Next"
    FONT_SIZE = 50
    HEART_SIZE = (40, 40)
    HEART_PADDING = 10
    INITIAL_LIVES = 3

    FONT_YOU_LOSE = pygame.font.Font('Emulogic-zrEw.ttf', 80)
    BUTTON_FONT = pygame.font.Font('Emulogic-zrEw.ttf', 30)

    def __init__(self, screen):
        self.screen = screen
        pygame.display.set_caption("Find the Error")
        self.font = pygame.font.SysFont("Inkfree", self.FONT_SIZE)
        self.next_button = Button(self.BUTTON_TEXT, 200, 40, self.BUTTON_POSITION, 5, self.font)
        self.restart_button_rect = pygame.Rect(400, 600, 400, 60)
        self.correct_sound = pygame.mixer.Sound("correct.mp3")
        self.incorrect_sound = pygame.mixer.Sound("wrong.mp3")

        self.heart_image = pygame.image.load('heart.png').convert_alpha()
        self.heart_image = pygame.transform.scale(self.heart_image, self.HEART_SIZE)

        self.codes = [
            "next :: Int -> Int\nnext n for (rem n 2) == 0 then (div n 2) else (3 * n + 1)\n\ncollatz :: Int -> [Int]\ncollatz 4 = [4, 2, 1]\ncollatz n = [n] ++ (collatz $ next n)\n\nmain = print $ collatz 7",
            "primes = filterPrime [2..] WHERE\n  filterPrime (p:xs) =\n  p : filterPrime [x | x <- xs, x `mod` p /= 0]",
            "import Data.List \n\nnumToDigits :: Int -> [Int]\nnumToDigits = map digitToInt . show"
        ]
        self.answers = ["for", "WHERE", "Data.List"]
        self.qcount = 0
        self.lives = self.INITIAL_LIVES
        self.game_over = False

    def display_text_and_get_answer(self, text, pos, font, color):
        words_surf = []
        words_pos = []
        words_text = []

        collection = [word.split(' ') for word in text.splitlines()]
        space = font.size(' ')[0]
        x, y = pos

        for lines in collection:
            for words in lines:
                word_surface = font.render(words, True, color)
                word_width, word_height = word_surface.get_size()

                if x + word_width >= self.SCREEN_WIDTH - 40:
                    x = pos[0]
                    y += word_height

                words_pos.append((x, y))
                words_surf.append(word_surface)
                words_text.append(words)
                x += word_width + space

            x = pos[0]
            y += word_height

        return words_text, words_pos, words_surf

    def get_question(self, text):
        words_text, words_pos, words_surf = self.display_text_and_get_answer(text, self.QUESTION_POSITION, self.font, pygame.Color('white'))
        return words_text, words_pos, words_surf

    def display_lives(self, lives):
        x = self.SCREEN_WIDTH - self.HEART_PADDING
        y = self.HEART_PADDING
        self.screen.blit(self.font.render('Lives:', True, pygame.Color('white')), (x - 300, y))
        x -= self.HEART_SIZE[0] + self.HEART_PADDING
        for _ in range(lives):
            self.screen.blit(self.heart_image, (x, y))
            x -= self.HEART_SIZE[0] + self.HEART_PADDING

    def draw_rounded_button(self, surface, color, rect, radius, text, font):
        pygame.draw.rect(surface, color, rect, border_radius=radius)
        text_surface = font.render(text, True, pygame.Color('black'))
        text_rect = text_surface.get_rect(center=rect.center)
        surface.blit(text_surface, text_rect)

    def text_up_down(self, text, y_position):
        input_surface = self.FONT_YOU_LOSE.render(text, True, pygame.Color('white'))
        for offset in range(0, 10, 5):
            self.screen.fill(pygame.Color('black'))
            self.draw_rounded_button(self.screen, pygame.Color('lightgrey'), self.restart_button_rect, 30, 'Restart Level', self.BUTTON_FONT)
            self.screen.blit(input_surface, (250, y_position + offset))
            pygame.display.flip()
            pygame.time.delay(150)

    def display_you_lose(self):
        while True:
            self.screen.fill(pygame.Color('black'))
            self.draw_rounded_button(self.screen, pygame.Color('lightgrey'), self.restart_button_rect, 30, 'Restart Level', self.BUTTON_FONT)
            self.text_up_down('You Lose', 250)
            self.draw_rounded_button(self.screen, pygame.Color('lightgrey'), self.restart_button_rect, 30, 'Restart Level', self.BUTTON_FONT)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.restart_button_rect.collidepoint(event.pos):
                        self.reset_game()
                        return

    def error_game(self, text, ans, lives):
        words, position, surface = self.get_question(text)
        answered_correctly = False
        while True:
            self.screen.fill(pygame.Color('black'))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if not answered_correctly:
                        for i in range(len(surface)):
                            button_rect = surface[i].get_rect(topleft=position[i])
                            if button_rect.collidepoint(event.pos):
                                if words[i] == ans:
                                    self.correct_sound.play()
                                    surface[i] = self.font.render(words[i], True, pygame.Color('green'))
                                    answered_correctly = True
                                    break
                                else:
                                    surface[i] = self.font.render(words[i], True, pygame.Color('red'))
                                    self.incorrect_sound.play()
                                    lives -= 1
                                    if lives <= 0:
                                        return lives, False
                    if self.next_button.top_rect.collidepoint(event.pos):
                        return lives, True

            for i in range(len(words)):
                self.screen.blit(surface[i], position[i])

            if answered_correctly:
                self.next_button.draw(self.screen)

            self.display_lives(lives)
            pygame.display.update()

    def run(self):
        while True:
            if not self.game_over:
                if self.qcount < len(self.codes):
                    self.lives, correct = self.error_game(self.codes[self.qcount], self.answers[self.qcount], self.lives)
                    if self.lives <= 0:
                        self.game_over = True
                    elif correct:
                        self.qcount += 1
                else:
                    break
            else:
                self.display_you_lose()
                pygame.display.update()

    def reset_game(self):
        self.qcount = 0
        self.lives = self.INITIAL_LIVES
        self.game_over = False
        self.run()

'''screen = pygame.display.set_mode((Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT))
game = Game(screen)
game.run()'''

