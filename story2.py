import pygame
import sys
import time
from flashcards import FlashcardsGame

class button:
    def __init__(self, text, width, height, pos, elevation, gui_font, screen):
        self.pressed = False
        self.elevation = elevation
        self.dynamic_elevation = elevation
        self.original_y_pos = pos[1]
        self.screen = screen

        self.top_rect = pygame.Rect(pos, (width, height))
        self.top_color = '#475F77'

        self.bottom_rect = pygame.Rect(pos, (width, height))
        self.bottom_color = '#354B5E'

        self.text_surf = gui_font.render(text, True, '#FFFFFF')
        self.text_rect = self.text_surf.get_rect(center=self.top_rect.center)

    def draw(self, screen):
        self.top_rect.y = self.original_y_pos - self.dynamic_elevation
        self.text_rect.center = self.top_rect.center

        self.bottom_rect.midtop = self.top_rect.midtop
        self.bottom_rect.height = self.top_rect.height + self.dynamic_elevation

        pygame.draw.rect(screen, self.bottom_color, self.bottom_rect, border_radius=12)
        pygame.draw.rect(screen, self.top_color, self.top_rect, border_radius=12)
        screen.blit(self.text_surf, self.text_rect)
        return self.check_click()

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.top_rect.collidepoint(mouse_pos):
            self.top_color = '#D74B4B'
            if pygame.mouse.get_pressed()[0]:
                self.dynamic_elevation = 0
                self.pressed = True
            else:
                self.dynamic_elevation = self.elevation
                if self.pressed:
                    self.pressed = False
                    return True
        else:
            self.dynamic_elevation = self.elevation
            self.top_color = '#475F77'
        return False


class BackgroundTransition:
    def __init__(self, screen):
        self.WIDTH, self.HEIGHT = 1200, 800
        self.BUTTON_POSITION = (self.WIDTH // 2 - 100, self.HEIGHT // 2 + 200)
        self.BUTTON_TEXT = "Start"
        self.FONT_SIZE = 50
        self.image_display_duration = 2
        self.black_screen_duration = 2
        self.fade_duration = 1
        self.alpha_step = 255 // (self.fade_duration * 10)
        self.screen = screen
        #self.screen = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
        self.font = pygame.font.SysFont("Inkfree", self.FONT_SIZE)

        self.button = button(self.BUTTON_TEXT, 200, 40, self.BUTTON_POSITION, 5, self.font, screen)

        self.background_images = [
            pygame.transform.scale(pygame.image.load('haunted_house.png').convert(), (self.WIDTH, self.HEIGHT)),
            pygame.transform.scale(pygame.image.load('inside_house.jpg').convert(), (self.WIDTH, self.HEIGHT)),
            pygame.transform.scale(pygame.image.load('room2.jpg').convert(), (self.WIDTH, self.HEIGHT))
        ]

        self.black_screen = pygame.Surface((self.WIDTH, self.HEIGHT))
        self.black_screen.fill((0, 0, 0))
        self.current_image_index = 0
        self.show_black_screen = False
        self.last_switch_time = time.time()
        self.background_count = 0
        self.start_clicked = False
        self.running = True

    def fade_in(self, image):
        overlay = pygame.Surface((self.WIDTH, self.HEIGHT))
        for alpha in range(0, 256, self.alpha_step):
            overlay.set_alpha(255 - alpha)
            self.screen.blit(image, (0, 0))
            self.screen.blit(overlay, (0, 0))
            pygame.display.flip()
            pygame.time.delay(int(self.fade_duration * 1000 / (255 / self.alpha_step)))

    def fade_out(self, image):
        overlay = pygame.Surface((self.WIDTH, self.HEIGHT))
        for alpha in range(0, 256, self.alpha_step):
            overlay.set_alpha(alpha)
            self.screen.blit(image, (0, 0))
            self.screen.blit(overlay, (0, 0))
            pygame.display.flip()
            pygame.time.delay(int(self.fade_duration * 1000 / (255 / self.alpha_step)))

    def handle_start_screen(self):
        while not self.start_clicked:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if self.button.top_rect.collidepoint(event.pos):
                        self.start_clicked = True

            self.screen.blit(self.background_images[0], (0, 0))
            self.button.draw(self.screen)
            pygame.display.flip()

    def handle_background_transitions(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            current_time = time.time()

            if self.show_black_screen and current_time - self.last_switch_time >= self.black_screen_duration:
                self.show_black_screen = False
                self.last_switch_time = current_time
                self.current_image_index = (self.current_image_index + 1) % len(self.background_images)
                self.background_count += 1
                self.fade_in(self.background_images[self.current_image_index])
            elif not self.show_black_screen and current_time - self.last_switch_time >= self.image_display_duration:
                self.show_black_screen = True
                self.last_switch_time = current_time
                self.fade_out(self.background_images[self.current_image_index])

            if self.show_black_screen:
                self.screen.blit(self.black_screen, (0, 0))
            else:
                self.screen.blit(self.background_images[self.current_image_index], (0, 0))

            pygame.display.flip()
            pygame.time.delay(100)

            if self.background_count >= 2 and self.show_black_screen:
                self.running = False

        #pygame.quit()
        #sys.exit()

    def run(self):
        self.handle_start_screen()
        self.handle_background_transitions()

'''pygame.init()
screen = pygame.display.set_mode((1200, 800))
game = BackgroundTransition(screen)
game.run()
game1 = FlashcardsGame(screen)
game1.run()'''

