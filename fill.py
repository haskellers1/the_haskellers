import pygame
import random
import time

class Fill_Game:
    def __init__(self,screen):
        self.screen_width = 1200
        self.screen_height = 800
        self.screen = screen
        #self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        pygame.display.set_caption('Fill Game')
        self.load_resources()

        self.font = pygame.font.Font('Emulogic-zrEw.ttf', 80)
        self.button_font = pygame.font.Font('Emulogic-zrEw.ttf', 30)

        self.game_played = False
        self.game_active = True

    def load_resources(self):
        self.background = pygame.image.load('bg.jpg').convert()
        self.background = pygame.transform.scale(self.background, (self.screen_width, self.screen_height))
        self.key_background = pygame.image.load('key.png').convert()
        self.key_background = pygame.transform.scale(self.key_background, (self.screen_width, self.screen_height))
        self.heart_image = pygame.image.load('heart.png').convert_alpha()
        self.heart_image = pygame.transform.scale(self.heart_image, (40, 40))
        self.correct_sound = pygame.mixer.Sound('correct.mp3')
        self.wrong_sound = pygame.mixer.Sound('wrong.mp3')

    def display_text(self, text, x, y, font_size=40, color=(0, 0, 0)):
        font = pygame.font.Font(None, font_size)
        text_surface = font.render(text, True, color)
        self.screen.blit(text_surface, (x, y))

    def display_code(self, code, x, y, font_size=34):
        font = pygame.font.Font(None, font_size)
        lines = code.split('\n')
        for i, line in enumerate(lines):
            text_surface = font.render(line, True, (0, 255, 255))
            self.screen.blit(text_surface, (x, y + i * (font_size + 2)))

    def display_hearts(self, x, y, lives):
        for i in range(lives):
            self.screen.blit(self.heart_image, (x + i * 50, y))

    def get_codes(self, file_path):
        with open(file_path, 'r') as file:
            content = file.read().strip()
            codes = content.split('\n\n\n')
        return codes

    def get_correct_words(self, file_path):
        with open(file_path, 'r') as file:
            words = file.read().strip().split('\n')
        return [word.strip() for word in words]

    def get_output(self, file_path):
        with open(file_path, 'r') as file:
            outputs = file.read().strip().split('\n\n')
        return outputs

    def show_message_screen(self, message):
        background_msg = pygame.image.load('hack_bg.jpg').convert()
        background_msg = pygame.transform.scale(background_msg, (self.screen_width, self.screen_height))
        self.screen.blit(background_msg, (0, 0))

        self.display_text(message, self.screen_width // 2 - 200, self.screen_height // 2, font_size=50,
                          color=(255, 255, 255))
        pygame.display.update()
        pygame.time.delay(2000)

    def draw_rounded_button(self, color, rect, radius, text):
        pygame.draw.rect(self.screen, color, rect, border_radius=radius)
        text_surface = self.button_font.render(text, True, pygame.Color('black'))
        text_rect = text_surface.get_rect(center=rect.center)
        self.screen.blit(text_surface, text_rect)

    def text_up_down(self, text, y_position):
        input_surface = self.font.render(text, True, pygame.Color('white'))
        for offset in range(-10, 10, 5):
            self.screen.fill(pygame.Color('black'))
            button_rect = pygame.Rect(400, 600, 400, 60)
            self.draw_rounded_button(pygame.Color('lightgrey'), button_rect, 30, 'Restart Level')
            self.screen.blit(input_surface, (250, y_position + offset))
            pygame.display.flip()
            pygame.time.delay(150)

    def display_you_lose(self):
        button_rect = pygame.Rect(400, 600, 400, 60)
        restart_requested = False

        while not restart_requested:
            self.screen.fill(pygame.Color('black'))
            self.draw_rounded_button(pygame.Color('lightgrey'), button_rect, 30, 'Restart Level')
            self.text_up_down('You Lose', 250)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return

                if event.type == pygame.MOUSEBUTTONDOWN:
                    mouse_pos = pygame.mouse.get_pos()
                    if button_rect.collidepoint(mouse_pos):
                        restart_requested = True
                        self.reset_game_state()
                        break

    def reset_game_state(self):
        self.game_played = False

    def play_game(self):
        while True:
            if not self.game_active:
                return

            if not self.game_played:
                self.game_played = True

                codes_file = 'codes.txt'
                correct_words_file = 'correct_words.txt'
                output_file = 'output.txt'

                codes = self.get_codes(codes_file)
                correct_words = self.get_correct_words(correct_words_file)
                outputs = self.get_output(output_file)

                self.show_message_screen("You are Hacking in the system!")

                self.screen.blit(self.background, (0, 0))
                pygame.display.update()

                rounds = len(codes)
                played_indices = set()

                for round_number in range(rounds):
                    while True:
                        index = random.randint(0, len(codes) - 1)
                        if index not in played_indices:
                            played_indices.add(index)
                            break

                    code = codes[index]
                    correct_word = correct_words[index]
                    output_text = outputs[index]

                    self.screen.blit(self.background, (0, 0))
                    self.display_code(code, 50, 100)
                    length_text = f"Length of the word to guess: {len(correct_word)}"
                    self.display_text(length_text, 50, 40, color=(255, 255, 255))
                    pygame.display.update()

                    lives = 3
                    end_of_game = False
                    time_up = False
                    lives_exhausted = False

                    input_text = ""
                    input_surface = pygame.Surface((self.screen_width, 30), pygame.SRCALPHA)
                    input_surface.fill((0, 0, 0, 0))
                    self.screen.blit(input_surface, (0, 450))
                    self.display_text(f"Enter your guess for the word: {input_text}", 50, 450, font_size=34,
                                      color=(0, 255, 146))
                    pygame.display.update()

                    start_time = time.time()
                    time_limit = 30
                    time_left = time_limit

                    while not end_of_game and not time_up:
                        time_elapsed = time.time() - start_time
                        time_left = max(time_limit - int(time_elapsed), 0)
                        self.screen.blit(self.background, (0, 0))
                        self.display_code(code, 50, 100)
                        self.display_text(length_text, 50, 40, color=(255, 255, 255))
                        self.display_text(f"Time left:", 840, 40, color=(255, 255, 255))
                        self.display_text(f"{time_left} seconds", 1000, 40, color=(255, 0, 0))
                        self.display_hearts(950, 80, lives)
                        self.display_text(f"Lives: ", 840, 90, color=(255, 255, 255))
                        self.display_text(f"Enter your guess for the word: {input_text}", 50, 600, font_size=34,
                                          color=(0, 255, 146))

                        pygame.display.update()

                        if time_left <= 0:
                            end_of_game = True
                            time_up = True
                            break

                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                return

                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_RETURN:
                                    guess = input_text.strip()

                                    if len(guess) != len(correct_word):
                                        self.display_text(f"Your guess length does not match. Try again.", 50, 700,
                                                          color=(255, 255, 255))
                                        pygame.display.update()
                                        pygame.time.delay(2000)
                                        continue

                                    if guess == correct_word:
                                        self.screen.blit(self.background, (0, 0))
                                        self.display_code(code, 50, 100)
                                        self.display_text(length_text, 50, 40, color=(255, 255, 255))
                                        self.display_text(f"Time left:", 840, 40, color=(255, 255, 255))
                                        self.display_text(f"{time_left} seconds", 1000, 40, color=(255, 0, 0))
                                        self.display_hearts(950, 80, lives)
                                        self.display_text(f"Lives: ", 840, 90, color=(255, 255, 255))
                                        self.display_text(f"Enter your guess for the word: {input_text}", 50, 600,
                                                          font_size=34, color=(0, 255, 146))
                                        self.display_text(output_text, 50, 700, color=(0, 255, 0))
                                        pygame.display.update()
                                        pygame.time.delay(1000)
                                        self.correct_sound.play()
                                        end_of_game = True
                                        break

                                    if lives > 0:
                                        self.display_text(f"'{guess}' is incorrect. Try again.", 50, 700,
                                                          color=(255, 0, 0))
                                        pygame.display.update()
                                        pygame.time.delay(1000)
                                        self.wrong_sound.play()
                                        lives -= 1

                                    if lives == 0:
                                        end_of_game = True
                                        lives_exhausted = True
                                        break

                                elif event.key == pygame.K_BACKSPACE:
                                    input_text = input_text[:-1]

                                elif event.key == pygame.K_ESCAPE:
                                    return

                                else:
                                    input_text += event.unicode

                    if time_up or lives_exhausted:
                        self.display_you_lose()

                self.game_active = False
                return


'''pygame.init()
game = Fill_Game()
game.play_game()'''

