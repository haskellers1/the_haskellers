using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class wrongscript : MonoBehaviour

{
    public Button wrong1;
    public Button wrong2;
    public Button wrong3;
    public Button correctAnswer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changeColor()
    {
        wrong1.onClick.AddListener(wrong1color);
        //wrong1.GetComponent<Image>().color = Color.red;
        //wrong2.GetComponent<Image>().color = Color.red;
        //wrong3.GetComponent<Image>().color = Color.red;
        wrong2.onClick.AddListener(wrong2color);
        wrong3.onClick.AddListener(wrong3color);
        correctAnswer.onClick.AddListener(CAcolor);
        void wrong1color()
        {
            wrong1.GetComponent<Image>().color = Color.red;
        }
        void wrong2color()
        {
            wrong2.GetComponent<Image>().color = Color.red;
        }
        void wrong3color()
        {
            wrong3.GetComponent<Image>().color = Color.red;
        }
        void CAcolor()
        {
            correctAnswer.GetComponent<Image>().color = Color.green;
        }
    }

}