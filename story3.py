import pygame
import sys
from fill import Fill_Game

class TriHaskGame:
    def __init__(self, screen):

        self.screen_width = 1200
        self.screen_height = 800
        self.screen = screen
        #self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        pygame.display.set_caption("TriHask")

        self.intro_image = pygame.image.load('story-bg.jpg')
        self.intro_image = pygame.transform.scale(self.intro_image, (self.screen_width, self.screen_height))
        self.background_image = pygame.image.load('story-bg1.jpg')
        self.background_image = pygame.transform.scale(self.background_image, (self.screen_width, self.screen_height))
        self.background_image2 = pygame.image.load('story-bg2.jpg')
        self.background_image2 = pygame.transform.scale(self.background_image2, (self.screen_width, self.screen_height))
        self.transition_image = pygame.image.load('lock2.jpg')
        self.transition_image = pygame.transform.scale(self.transition_image, (self.screen_width, self.screen_height))
        self.lock2_image = pygame.image.load('window.png')
        self.lock2_image = pygame.transform.scale(self.lock2_image, (self.screen_width, self.screen_height))

        self.popup_bg = pygame.image.load('popup_bg.jpg').convert_alpha()
        self.popup_bg = pygame.transform.scale(self.popup_bg, (400, 200))

        self.font = pygame.font.Font(None, 36)
        self.text_surface = self.font.render("Enter the door", True, (255, 255, 255))
        self.popup_bg.blit(self.text_surface, (200 - self.text_surface.get_width() // 2, 50))

        self.button_image = pygame.Surface((150, 50), pygame.SRCALPHA)
        self.button_color = (255, 255, 255, 200)
        self.button_pressed_color = (200, 200, 200, 200)
        pygame.draw.rect(self.button_image, self.button_color, (0, 0, 150, 50))
        pygame.draw.rect(self.button_image, (0, 0, 0), (0, 0, 150, 50), 2)
        pygame.draw.line(self.button_image, (255, 255, 255, 50), (1, 1), (148, 1), 2)
        pygame.draw.line(self.button_image, (255, 255, 255, 50), (1, 1), (1, 48), 2)
        pygame.draw.line(self.button_image, (100, 100, 100, 100), (2, 49), (148, 49), 2)
        pygame.draw.line(self.button_image, (100, 100, 100, 100), (148, 1), (148, 49), 2)
        self.button_text = self.font.render("Enter", True, (0, 0, 0))
        self.button_image.blit(self.button_text, (75 - self.button_text.get_width() // 2, 15))

        self.button_pressed_image = pygame.Surface((150, 50), pygame.SRCALPHA)
        pygame.draw.rect(self.button_pressed_image, self.button_pressed_color, (0, 0, 150, 50))
        pygame.draw.rect(self.button_pressed_image, (0, 0, 0), (0, 0, 150, 50), 2)
        pygame.draw.line(self.button_pressed_image, (255, 255, 255, 50), (1, 1), (148, 1), 2)
        pygame.draw.line(self.button_pressed_image, (255, 255, 255, 50), (1, 1), (1, 48), 2)
        pygame.draw.line(self.button_pressed_image, (100, 100, 100, 100), (2, 49), (148, 49), 2)
        pygame.draw.line(self.button_pressed_image, (100, 100, 100, 100), (148, 1), (148, 49), 2)
        self.button_pressed_image.blit(self.button_text, (75 - self.button_text.get_width() // 2, 15))

        self.popup_rect = self.popup_bg.get_rect(center=(self.screen_width // 2, self.screen_height // 2))
        self.button_rect = self.button_image.get_rect(center=(self.screen_width // 2, self.screen_height // 2 + 60))

        self.hack_button_surface = pygame.Surface((150, 50), pygame.SRCALPHA)
        self.hack_button_color = (255, 255, 255, 200)
        self.hack_button_pressed_color = (200, 200, 200, 200)
        pygame.draw.rect(self.hack_button_surface, self.hack_button_color, (0, 0, 150, 50))
        pygame.draw.rect(self.hack_button_surface, (0, 0, 0), (0, 0, 150, 50), 2)
        pygame.draw.line(self.hack_button_surface, (255, 255, 255, 50), (1, 1), (148, 1), 2)
        pygame.draw.line(self.hack_button_surface, (255, 255, 255, 50), (1, 1), (1, 48), 2)
        pygame.draw.line(self.hack_button_surface, (100, 100, 100, 100), (2, 49), (148, 49), 2)
        pygame.draw.line(self.hack_button_surface, (100, 100, 100, 100), (148, 1), (148, 49), 2)
        self.hack_button_text = self.font.render("Hack", True, (0, 0, 0))
        self.hack_button_surface.blit(self.hack_button_text, (75 - self.hack_button_text.get_width() // 2, 15))
        self.hack_button_rect = self.hack_button_surface.get_rect(center=(self.screen_width // 2, self.screen_height // 2 + 70))

        self.go_back_button_surface = pygame.Surface((150, 50), pygame.SRCALPHA)
        self.go_back_button_color = (255, 255, 255, 200)
        self.go_back_button_pressed_color = (200, 200, 200, 200)
        pygame.draw.rect(self.go_back_button_surface, self.go_back_button_color, (0, 0, 150, 50))
        pygame.draw.rect(self.go_back_button_surface, (0, 0, 0), (0, 0, 150, 50), 2)
        pygame.draw.line(self.go_back_button_surface, (255, 255, 255, 50), (1, 1), (148, 1), 2)
        pygame.draw.line(self.go_back_button_surface, (255, 255, 255, 50), (1, 1), (1, 48), 2)
        pygame.draw.line(self.go_back_button_surface, (100, 100, 100, 100), (2, 49), (148, 49), 2)
        pygame.draw.line(self.go_back_button_surface, (100, 100, 100, 100), (148, 1), (148, 49), 2)
        self.go_back_button_text = self.font.render("BACK", True, (0, 0, 0))
        self.go_back_button_surface.blit(self.go_back_button_text, (75 - self.go_back_button_text.get_width() // 2, 15))
        self.go_back_button_rect = self.go_back_button_surface.get_rect(bottomright=(self.screen_width - 20, self.screen_height - 20))

        self.back_button_surface_transition = pygame.Surface((150, 50), pygame.SRCALPHA)
        pygame.draw.rect(self.back_button_surface_transition, self.go_back_button_color, (0, 0, 150, 50))
        pygame.draw.rect(self.back_button_surface_transition, (0, 0, 0), (0, 0, 150, 50), 2)
        pygame.draw.line(self.back_button_surface_transition, (255, 255, 255, 50), (1, 1), (148, 1), 2)
        pygame.draw.line(self.back_button_surface_transition, (255, 255, 255, 50), (1, 1), (1, 48), 2)
        pygame.draw.line(self.back_button_surface_transition, (100, 100, 100, 100), (2, 49), (148, 49), 2)
        pygame.draw.line(self.back_button_surface_transition, (100, 100, 100, 100), (148, 1), (148, 49), 2)
        self.back_button_text_transition = self.font.render("Back", True, (0, 0, 0))
        self.back_button_surface_transition.blit(self.back_button_text_transition,
                                                (75 - self.back_button_text_transition.get_width() // 2, 15))
        self.back_button_rect_transition = self.back_button_surface_transition.get_rect(
            bottomright=(self.screen_width - 20, self.screen_height - 20))

        self.current_background = self.intro_image

        self.show_hack_message_delay = 2000
        self.hack_message_shown = False
        self.hack_message_start_time = 0
        self.hack_button_alpha = 0
        self.hack_button_fade_speed = 5
        self.fade_speed = 5
        self.fade_alpha = 0
        self.fade_in = True
        self.fade_out = False
        self.transitioning = False

        self.popup_active = False
        self.show_message = False
        self.show_message_delay = 1000
        self.start_time = pygame.time.get_ticks()
        self.transition_alpha = 0
        self.show_transition_image = False
        self.hack_button_pressed = False
        self.lock2_transitioning = False
        self.switch_to_window = False
        self.button_pressed = False
        self.intro_image_shown = False

    def draw_translucent_box(self, text):
        translucent_box = pygame.Surface((400, 200), pygame.SRCALPHA)
        translucent_box.fill((0, 0, 0, 180))
        translucent_rect = translucent_box.get_rect(center=(self.screen_width // 2, self.screen_height // 2))
        self.screen.blit(translucent_box, translucent_rect)
        yay_surface = self.font.render(text, True, (255, 255, 255))
        self.screen.blit(yay_surface, (translucent_rect.centerx - yay_surface.get_width() // 2,
                                       translucent_rect.centery - yay_surface.get_height() // 2))

    def run(self):
        running = True
        while running:
            current_time = pygame.time.get_ticks()
            time_elapsed = current_time - self.start_time

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    x_pos, y_pos = event.pos
                    self.button_pressed = False

                    if self.current_background == self.intro_image:
                        self.current_background = self.background_image
                        self.intro_image_shown = True
                        self.start_time = pygame.time.get_ticks()
                        continue

                    if self.current_background == self.background_image2 and 500 < x_pos < 900 and 200 < y_pos < 700:
                        self.current_background = self.transition_image
                        self.show_transition_image = True
                        self.transitioning = False
                        self.hack_button_pressed = False
                        self.hack_message_shown = False
                        self.fade_alpha = 0
                        self.fade_in = True
                        self.fade_out = False

                    if self.current_background == self.background_image and self.popup_active and self.button_rect.collidepoint(event.pos):
                        self.button_pressed = True
                    elif self.current_background == self.transition_image and self.back_button_rect_transition.collidepoint(event.pos):
                        self.current_background = self.background_image2
                        self.show_transition_image = False
                        self.transitioning = False
                        self.hack_button_pressed = False
                        self.fade_alpha = 255
                        self.fade_in = False
                        self.fade_out = True
                    elif self.current_background == self.transition_image and self.button_pressed and self.button_rect.collidepoint(event.pos):
                        self.button_pressed = False
                        self.hack_button_pressed = True
                        self.popup_active = False
                        self.transitioning = True
                    elif self.hack_button_rect.collidepoint(event.pos):
                        self.hack_button_pressed = True
                        game_instance = Fill_Game(self.screen)
                        game_instance.play_game()
                        return False
                    elif self.current_background == self.background_image2 and 5 < x_pos < 540 and 250 < y_pos < 700:
                        self.switch_to_window = True
                        self.lock2_transitioning = True
                    elif self.current_background == self.lock2_image and self.go_back_button_rect.collidepoint(event.pos):
                        self.current_background = self.background_image2

                elif event.type == pygame.MOUSEBUTTONUP:
                    if self.button_pressed and self.button_rect.collidepoint(event.pos):
                        self.button_pressed = False
                        self.popup_active = False
                        self.transitioning = True
                    elif self.show_transition_image and self.hack_button_rect.collidepoint(event.pos):
                        game_instance = Fill_Game(self.screen)
                        game_instance.play_game()
                        self.show_transition_image = False
                        self.hack_button_pressed = False

            if self.current_background == self.background_image:
                self.popup_active = True
            else:
                self.popup_active = False

            self.screen.blit(self.current_background, (0, 0))

            if self.show_transition_image:
                self.screen.blit(self.transition_image, (0, 0))
                if not self.hack_message_shown and current_time - self.start_time >= self.show_hack_message_delay:
                    self.hack_message_shown = True
                    self.hack_message_start_time = current_time
                if self.hack_message_shown:
                    self.draw_translucent_box("You will have to hack to open it!!")
                    if self.hack_button_alpha < 255:
                        self.hack_button_alpha += self.hack_button_fade_speed
                        self.hack_button_surface.set_alpha(self.hack_button_alpha)
                    self.screen.blit(self.hack_button_surface, self.hack_button_rect)
                    self.screen.blit(self.back_button_surface_transition, self.back_button_rect_transition)

            if self.popup_active and not self.show_transition_image:
                self.screen.blit(self.popup_bg, self.popup_rect)
                if self.button_pressed:
                    self.screen.blit(self.button_pressed_image, self.button_rect)
                else:
                    self.screen.blit(self.button_image, self.button_rect)

            if self.hack_button_pressed and not self.show_transition_image:
                if pygame.mouse.get_pressed()[0]:
                    self.screen.blit(self.hack_button_pressed_image, self.hack_button_rect)
                else:
                    self.screen.blit(self.hack_button_surface, self.hack_button_rect)

            if self.transitioning:
                if self.fade_in:
                    if self.fade_alpha < 255:
                        self.fade_alpha += self.fade_speed
                    else:
                        self.fade_alpha = 255
                        self.fade_in = False
                        self.transitioning = False
                        self.current_background = self.background_image2
                        self.transition_alpha = 0
                elif self.fade_out:
                    if self.fade_alpha > 0:
                        self.fade_alpha -= self.fade_speed
                    else:
                        self.fade_alpha = 0
                        self.fade_out = False
                        self.show_transition_image = False
                        self.current_background = self.background_image2
                        self.transition_alpha = 0
                transition_surface = pygame.Surface((self.screen_width, self.screen_height))
                transition_surface.fill((0, 0, 0))
                transition_surface.set_alpha(self.fade_alpha)
                self.screen.blit(transition_surface, (0, 0))

            if self.current_background == self.background_image2 and not self.show_transition_image and not self.transitioning:
                if not self.show_message and time_elapsed >= self.show_message_delay:
                    self.show_message = True

            if self.show_message and not self.show_transition_image and self.current_background != self.lock2_image and self.current_background != self.transition_image:
                translucent_box = pygame.Surface((400, 200), pygame.SRCALPHA)
                translucent_box.fill((0, 0, 0, 180))
                translucent_rect = translucent_box.get_rect(center=(self.screen_width // 2, self.screen_height // 2))
                self.screen.blit(translucent_box, translucent_rect)
                yay_surface = self.font.render("Find the lock to secret room.", True, (255, 255, 255))
                self.screen.blit(yay_surface, (translucent_rect.centerx - yay_surface.get_width() // 2,
                                              translucent_rect.centery - yay_surface.get_height() // 2))

            if self.switch_to_window:
                if self.lock2_transitioning:
                    if self.fade_alpha < 255:
                        self.fade_alpha += self.fade_speed
                    else:
                        self.fade_alpha = 255
                        self.lock2_transitioning = False
                        self.current_background = self.lock2_image
                        self.fade_alpha = 0
                    transition_surface = pygame.Surface((self.screen_width, self.screen_height))
                    transition_surface.fill((0, 0, 0))
                    transition_surface.set_alpha(self.fade_alpha)
                    self.screen.blit(transition_surface, (0, 0))
                else:
                    self.switch_to_window = False

            if self.current_background == self.transition_image:
                self.screen.blit(self.back_button_surface_transition, self.back_button_rect_transition)
            elif self.current_background == self.lock2_image:
                self.screen.blit(self.go_back_button_surface, self.go_back_button_rect)

            pygame.display.flip()

	#pygame.quit()
	#sys.exit()
'''game = TriHaskGame()
game.run()
'''
