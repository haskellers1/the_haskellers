import pygame
import sys
import time
import random
from phone import PhoneLockScreen

class Start:
    def __init__(self, screen):
        self.screen_width = 1200
        self.screen_height = 800
        self.screen = screen
        pygame.display.set_caption('TriHask')

        self.background_image = pygame.image.load('initial_room.jpg')
        self.background_image = pygame.transform.scale(self.background_image, (self.screen_width, self.screen_height))

        self.new_background_image = pygame.image.load('initial_room2.jpg')
        self.new_background_image = pygame.transform.scale(self.new_background_image, (self.screen_width, self.screen_height))

        self.font = pygame.font.Font(None, 36)

        self.message_background = pygame.Surface((self.screen_width, 100))
        self.message_background.set_alpha(128)
        self.message_background.fill((0, 0, 0))

        self.pov_message_background = pygame.Surface((self.screen_width, 50))
        self.pov_message_background.set_alpha(128)
        self.pov_message_background.fill((0, 0, 0))

        self.initial_message = "You are a Hacker by Profession."
        self.updated_message = "Enjoying a nice day in your living room."
        self.notification_message = "You have got a notification!!."
        self.pov_message = "Shit! what happened!"
        self.current_message = self.initial_message

        self.start_time = time.time()
        self.message_change_time = 2
        self.background_change_time = 5
        self.shake_duration = 2
        self.phone_call_delay = 2
        self.fade_duration = 1

        self.shaking = False
        self.show_pov_message = False
        self.show_notification = False
        self.notification_display_time = None
        self.phone_call_triggered = False
        self.fade_in = False
        self.fade_out = False
        self.fade_start_time = None
        self.show_updated_message = False
        self.show_new_background = False

        self.fade_surface = pygame.Surface((self.screen_width, self.screen_height))
        self.fade_surface.fill((0, 0, 0))

    def crossfade(self, alpha):
        self.fade_surface.set_alpha(alpha)
        self.screen.blit(self.fade_surface, (0, 0))

    def update(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            elapsed_time = time.time() - self.start_time
            fade_elapsed_time = time.time() - self.fade_start_time if self.fade_start_time else 0

            if elapsed_time < self.background_change_time:
                self.screen.blit(self.background_image, (0, 0))
            else:
                if not self.show_new_background:
                    self.show_new_background = True

                if elapsed_time < self.background_change_time + self.shake_duration:
                    shake_offset_x = random.randint(-10, 10)
                    shake_offset_y = random.randint(-10, 10)
                    self.screen.blit(self.new_background_image, (shake_offset_x, shake_offset_y))
                else:
                    self.screen.blit(self.new_background_image, (0, 0))
                    self.shaking = False
                    if not self.show_pov_message:
                        self.show_pov_message = True
                        self.pov_message_display_time = time.time()

            if self.show_pov_message:
                pov_text = self.font.render(self.pov_message, True, (255, 255, 255))
                self.screen.blit(self.pov_message_background, (0, 0))
                self.screen.blit(pov_text, (20, 10))

                if self.pov_message_display_time and time.time() - self.pov_message_display_time > 2:
                    self.show_notification = True
                    self.notification_display_time = time.time()
                    self.show_pov_message = False

            if elapsed_time <= self.message_change_time and not self.show_new_background:
                text = self.font.render(self.initial_message, True, (255, 255, 255))
            else:
                if self.show_new_background:
                    self.show_updated_message = False
                else:
                    self.show_updated_message = True

                if self.show_updated_message:
                    text = self.font.render(self.updated_message, True, (255, 255, 255))
                else:
                    text = self.font.render(self.initial_message, True, (255, 255, 255))

            if elapsed_time <= self.message_change_time and not self.show_new_background:
                self.screen.blit(self.message_background, (0, self.screen_height - 100))
                self.screen.blit(text, (20, self.screen_height - 80))

            if self.show_updated_message:
                self.screen.blit(self.message_background, (0, self.screen_height - 100))
                self.screen.blit(text, (20, self.screen_height - 80))

            if self.show_notification:
                notification_text = self.font.render(self.notification_message, True, (255, 255, 255))
                self.screen.blit(self.message_background, (0, self.screen_height - 100))
                self.screen.blit(notification_text, (20, self.screen_height - 80))

                if self.notification_display_time and time.time() - self.notification_display_time > self.phone_call_delay and not self.phone_call_triggered:
                    self.fade_start_time = time.time()
                    self.fade_out = True
                    self.phone_call_triggered = True

            if self.fade_out:
                fade_alpha = 255 * (1 - fade_elapsed_time / self.fade_duration)
                self.crossfade(fade_alpha)

                if fade_elapsed_time >= self.fade_duration:
                    phone_lock_screen = PhoneLockScreen(self.screen)
                    phone_lock_screen.run()
                    running = False

            pygame.display.flip()

'''pygame.init()
screen = pygame.display.set_mode((1200, 800))
game = Start(screen)
game.update()'''

