import pygame
import sys
import time

# Initialize Pygame
pygame.init()

# Set up display
WIDTH, HEIGHT = 1200, 800
screen = pygame.display.set_mode((WIDTH, HEIGHT))

# Load and scale background images
background_images = [
    pygame.transform.scale(pygame.image.load('inside_house.jpg').convert(), (WIDTH, HEIGHT)),
    pygame.transform.scale(pygame.image.load('door.jpg').convert(), (WIDTH, HEIGHT)),
    pygame.transform.scale(pygame.image.load('room.jpg').convert(), (WIDTH, HEIGHT))
]

# Initialize variables
black_screen = pygame.Surface((WIDTH, HEIGHT))
black_screen.fill((0, 0, 0))
current_image_index = 0
show_black_screen = False
last_switch_time = time.time()
background_count = 0  # Track the number of background images shown

# Time intervals
image_display_duration = 2  # seconds
black_screen_duration = 2   # seconds
fade_duration = 1  # seconds
alpha_step = 255 // (fade_duration * 10)  # 255 levels of alpha divided by the fade duration in tenths of a second

def fade_in(screen, image, duration=fade_duration):
    overlay = pygame.Surface((WIDTH, HEIGHT))
    for alpha in range(0, 256, alpha_step):
        overlay.set_alpha(255 - alpha)
        screen.blit(image, (0, 0))
        screen.blit(overlay, (0, 0))
        pygame.display.flip()
        pygame.time.delay(int(duration * 1000 / (255 / alpha_step)))

def fade_out(screen, image, duration=fade_duration):
    overlay = pygame.Surface((WIDTH, HEIGHT))
    for alpha in range(0, 256, alpha_step):
        overlay.set_alpha(alpha)
        screen.blit(image, (0, 0))
        screen.blit(overlay, (0, 0))
        pygame.display.flip()
        pygame.time.delay(int(duration * 1000 / (255 / alpha_step)))

# Main loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    current_time = time.time()

    # Check if it's time to switch
    if show_black_screen and current_time - last_switch_time >= black_screen_duration:
        show_black_screen = False
        last_switch_time = current_time
        current_image_index = (current_image_index + 1) % len(background_images)
        background_count += 1  # Increment the background count
        fade_in(screen, background_images[current_image_index])
    elif not show_black_screen and current_time - last_switch_time >= image_display_duration:
        show_black_screen = True
        last_switch_time = current_time
        fade_out(screen, background_images[current_image_index])

    # Display the appropriate screen
    if show_black_screen:
        screen.blit(black_screen, (0, 0))
    else:
        screen.blit(background_images[current_image_index], (0, 0))

    pygame.display.flip()
    pygame.time.delay(100)

    # Stop after the screen goes black after the third background
    if background_count >= 2 and show_black_screen:
        running = False

# Quit Pygame
pygame.quit()
sys.exit()
  
