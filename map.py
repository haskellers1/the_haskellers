import pygame
import sys
import math
from after_map import after_map

class map:
    def __init__(self, screen):
        

        self.screen_width = 1200
        self.screen_height = 800
        #self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        self.screen = screen
        self.background_image_path = 'map.jpg'
        self.pointer_image_path = 'pointer.png'

        self.background_image = pygame.image.load(self.background_image_path)
        self.background_image = pygame.transform.scale(self.background_image, (self.screen_width, self.screen_height))

        self.pointer_image = pygame.image.load(self.pointer_image_path)
        self.pointer_image = pygame.transform.scale(self.pointer_image, (50, 50))

        self.x_pos = 400
        self.y_base = 300

        self.amplitude = 10
        self.frequency = 0.01

        self.clock = pygame.time.Clock()

        self.pointer_rect = self.pointer_image.get_rect(topleft=(self.x_pos, self.y_base))

        self.running = True
        self.zoom_effect_triggered = False

    def run(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if self.pointer_rect.collidepoint(event.pos):
                        self.trigger_zoom_effect()

            y_pos = self.y_base + self.amplitude * math.sin(pygame.time.get_ticks() * self.frequency)

            self.screen.blit(self.background_image, (0, 0))
            self.screen.blit(self.pointer_image, (self.x_pos, y_pos))

            pygame.display.flip()
            self.clock.tick(60)

            if self.zoom_effect_triggered:
                self.run_zoom_effect()
                break

    def trigger_zoom_effect(self):
        self.zoom_effect_triggered = True

    def run_zoom_effect(self):
        fade_surface = pygame.Surface((self.screen_width, self.screen_height))
        fade_surface.fill((0, 0, 0))
        fade_alpha = 0
        fade_speed = 5

        while fade_alpha < 255:
            self.screen.blit(self.background_image, (0, 0))
            self.screen.blit(self.pointer_image, (self.x_pos, self.y_base + self.amplitude * math.sin(pygame.time.get_ticks() * self.frequency)))
            fade_surface.set_alpha(fade_alpha)
            self.screen.blit(fade_surface, (0, 0))
            pygame.display.flip()
            fade_alpha += fade_speed
            pygame.time.delay(10)

        self.run_after_map()

    def run_after_map(self):
        effect = after_map(self.screen)
        effect.run()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                return

            pygame.time.delay(10)

        self.running = False

# Initialize and run the animation
'''
pygame.init()
animation = Map()
animation.run()
pygame.quit()'''
