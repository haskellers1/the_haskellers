import pygame
import time
from button import Button

class Error_Game():
    def __init__(self, screen):
        self.screen_width = 1200
        self.screen_height = 800
        self.button_pos = (900,700)
        self.ques_pos = (40, 40)
        self.button_text = "Next"
        self.font_size = 50
        self.button_width = 200
        self.button_height = 40
        self.button_elevation = 5
        self.font_name = "Inkfree"
        self.screen = screen
        #self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        #pygame.display.set_caption("Find the Error")
        self.font = pygame.font.SysFont(self.font_name, self.font_size)
        self.next_button = Button(self.button_text, self.button_width, self.button_height, self.button_pos, self.button_elevation, self.font)

        # Quiz data
        self.codes = [
            "next :: Int -> Int\nnext n for (rem n 2) == 0 then (div n 2) else (3 * n + 1)\n\ncollatz :: Int -> [Int]\ncollatz 4 = [4, 2, 1]\ncollatz n = [n] ++ (collatz $ next n)\n\nmain = print $ collatz 7",
            "primes = filterPrime [2..] WHERE\n  filterPrime (p:xs) =\n  p : filterPrime [x | x <- xs, x `mod` p /= 0]",
            "import Data.List \n\nnumToDigits :: Int -> [Int]\nnumToDigits = map digitToInt . show"
        ]
        self.answers = ["for", "WHERE", "Data.List"]
        self.qcount = 0

    def display_text_and_get_answer(self, text, color):
        words_surf = []
        words_pos = []
        words_text = []
        
        collection = [word.split(' ') for word in text.splitlines()]
        space = self.font.size(' ')[0]
        x, y = self.ques_pos

        for lines in collection:
            for words in lines:
                word_surface = self.font.render(words, True, color)
                word_width, word_height = word_surface.get_size()
                
                if x + word_width >= self.screen_width - 40:
                    x = self.ques_pos[0]
                    y += word_height
                
                words_pos.append((x, y))
                words_surf.append(word_surface)
                words_text.append(words)
                x += word_width + space
            
            x = self.ques_pos[0]
            y += word_height
        
        return words_text, words_pos, words_surf

    def get_question(self, text):
        words_text, words_pos, words_surf = self.display_text_and_get_answer(text, pygame.Color('white'))
        return words_text, words_pos, words_surf
    
    def error_game(self, text, ans):
        words, position, surface = self.get_question(text)
        answered_correctly = False
        while True:
            pygame.display.update()
            self.screen.fill(pygame.Color('black'))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return False
                elif event.type == pygame.MOUSEBUTTONDOWN: 
                    if not answered_correctly:
                        for i in range(len(surface)):
                            button_rect = surface[i].get_rect(topleft=position[i])
                            if button_rect.collidepoint(event.pos):
                                if words[i] == ans:
                                    surface[i] = self.font.render(words[i], True, pygame.Color('green'))
                                    answered_correctly = True
                                    break
                                else:
                                    surface[i] = self.font.render(words[i], True, pygame.Color('red'))
                    if self.next_button.top_rect.collidepoint(event.pos):
                        return True
                    
            for i in range(len(words)):
                self.screen.blit(surface[i], position[i])
            
            if answered_correctly:
                self.next_button.draw(self.screen)

    def play_game(self):
        while self.qcount < len(self.codes):
            if self.qcount == len(self.codes):
                break
            correct = self.error_game(self.codes[self.qcount], self.answers[self.qcount])
            if correct:
                self.qcount += 1
            

'''pygame.init()
game_instance = Error_Game()
game_instance.play_game()'''