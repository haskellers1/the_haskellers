import pygame
import sys
from map import map

class after_start:
    def __init__(self, screen):

        self.screen_width = 1200
        self.screen_height = 800
        #self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        pygame.display.set_caption('TriHak')
        self.screen = screen
        self.background_image = pygame.image.load('initial_room2.jpg')
        self.background_image = pygame.transform.scale(self.background_image, (self.screen_width, self.screen_height))

        self.text_color = (255, 255, 255)
        self.font = pygame.font.Font(None, 36)

        self.initial_message_text = 'I guess I can get to know after going there only.'
        self.initial_text_surface = self.font.render(self.initial_message_text, True, self.text_color)
        self.initial_text_rect = self.initial_text_surface.get_rect(center=(self.screen_width / 2, self.screen_height - 75))

        self.second_message_text = 'Gotta go then!'
        self.second_text_surface = self.font.render(self.second_message_text, True, self.text_color)
        self.second_text_rect = self.second_text_surface.get_rect(center=(self.screen_width / 2, self.screen_height - 75))

        self.popup_width = 400
        self.popup_height = 200
        self.popup_x = (self.screen_width - self.popup_width) // 2
        self.popup_y = (self.screen_height - self.popup_height) // 2
        self.popup_box = pygame.Surface((self.popup_width, self.popup_height), pygame.SRCALPHA)
        pygame.draw.rect(self.popup_box, (30, 30, 30, 230), (0, 0, self.popup_width, self.popup_height), border_radius=20)

        self.popup_message_text = 'Go to location'
        self.popup_font = pygame.font.Font(None, 28)
        self.popup_text_surface = self.popup_font.render(self.popup_message_text, True, self.text_color)
        self.popup_text_rect = self.popup_text_surface.get_rect(center=(self.popup_width // 2, self.popup_height // 2 - 30))

        self.button_width = 120
        self.button_height = 50
        self.button_x = (self.popup_width - self.button_width) // 2
        self.button_y = (self.popup_height - self.button_height) // 2 + 30
        self.button_corner_radius = 10
        self.button_rect = pygame.Rect(self.popup_x + self.button_x, self.popup_y + self.button_y, self.button_width, self.button_height)

        self.running = True
        self.show_popup = False
        self.show_second_message = False
        self.initial_message_time = pygame.time.get_ticks()
        self.second_message_time = self.initial_message_time + 2000

    def run(self):
        while self.running:
            current_time = pygame.time.get_ticks()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    if self.show_popup and self.button_rect.collidepoint(event.pos):
                        self.handle_map_button_click()

            self.screen.blit(self.background_image, (0, 0))

            if current_time < self.second_message_time:
                self.display_translucent_message(self.initial_text_surface, self.initial_text_rect)
            elif current_time >= self.second_message_time and current_time < self.second_message_time + 2000:
                self.show_second_message = True

            if self.show_second_message:
                self.display_translucent_message(self.second_text_surface, self.second_text_rect)

            if current_time > self.second_message_time + 2000:
                self.show_popup = True

            if self.show_popup:
                self.display_popup()

            pygame.display.flip()

            if not self.running:
                break

    def display_translucent_message(self, text_surface, text_rect):
        translucent_surface = pygame.Surface((self.screen_width, 100), pygame.SRCALPHA)
        translucent_surface.fill((0, 0, 0, 150))
        self.screen.blit(translucent_surface, (0, self.screen_height - 100))
        self.screen.blit(text_surface, text_rect.topleft)

    def display_popup(self):
        self.screen.blit(self.popup_box, (self.popup_x, self.popup_y))
        self.screen.blit(self.popup_text_surface, (self.popup_x + self.popup_text_rect.x, self.popup_y + self.popup_text_rect.y))

        mouse_pos = pygame.mouse.get_pos()
        button_color_current = (34, 139, 34) if self.button_rect.collidepoint(mouse_pos) else (50, 205, 50)
        pygame.draw.rect(self.screen, button_color_current, self.button_rect, border_radius=self.button_corner_radius)

        button_text_surface = self.font.render('MAP', True, self.text_color)
        button_text_rect = button_text_surface.get_rect(center=self.button_rect.center)
        self.screen.blit(button_text_surface, button_text_rect.topleft)

    def handle_map_button_click(self):
        self.fade_out_and_run_animation()

    def fade_out_and_run_animation(self):
        fade_surface = pygame.Surface((self.screen_width, self.screen_height))
        fade_surface.fill((0, 0, 0))
        fade_alpha = 0
        fade_speed = 5

        while fade_alpha < 255:
            self.screen.blit(self.background_image, (0, 0))
            fade_surface.set_alpha(fade_alpha)
            self.screen.blit(fade_surface, (0, 0))
            pygame.display.flip()
            fade_alpha += fade_speed
            pygame.time.delay(10)

        # Run the map animation
        animation = map(self.screen)
        animation.run()
        self.running = False
        return

        # Exit the application
        

# Run the application
'''pygame.init()
start = after_start()
start.run()
pygame.quit()
sys.exit()'''