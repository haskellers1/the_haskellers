import pygame
import sys
from level2 import VideoPlayer

class Ending:
    def __init__(self, screen, pin):
        self.screen = screen
        self.digital_font = pygame.font.Font('digital-7.ttf', 150)
        self.font = pygame.font.Font('Emulogic-zrEw.ttf', 100)
        self.pin = pin
        self.background_image = pygame.image.load("locker.jpg")
        self.input_text = ""

    def check_ans(self):
        if self.input_text == self.pin:
            self.ending_page()
        else:
            self.screen.blit(self.background_image, (0, 0))
            invalid_surface = self.digital_font.render("INVALID", True, pygame.Color('red'))
            self.screen.blit(invalid_surface, (380, 120))
            pygame.display.update()
            pygame.time.delay(1000)
            self.take_input()

    def take_input(self):
        self.input_text = ""
        self.screen.blit(self.background_image, (0, 0))
        pygame.display.update()
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        self.check_ans()
                    elif event.key == pygame.K_BACKSPACE:
                        self.input_text = self.input_text[:-1]
                    else:
                        self.input_text += event.unicode
                self.screen.blit(self.background_image, (0, 0))
                input_surface = self.digital_font.render(self.input_text, True, pygame.Color('red'))
                self.screen.blit(input_surface, (380, 120))
                pygame.display.update()
                pygame.time.delay(100)
                self.screen.blit(self.background_image, (0, 0))
                input_surface = self.digital_font.render("* " * len(self.input_text), True, pygame.Color('red'))
                self.screen.blit(input_surface, (380, 125))
                pygame.display.update()

    def text_up_down(self, text):
        input_surface = self.font.render(text, True, pygame.Color('white'))
        self.screen.blit(input_surface, (200, 300))
        pygame.display.update()
        pygame.time.delay(300)
        self.screen.fill(pygame.Color('black'))
        self.screen.blit(input_surface, (200, 295))
        pygame.display.update()
        pygame.time.delay(300)

    def ending_page(self):
        video_player = VideoPlayer('locker_open.mp4')
        video_player.play(self.screen)
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                else:
                    self.screen.fill(pygame.Color('black'))
                    self.text_up_down("You win!")

    def run(self):
        self.take_input()
        
'''pygame.init()
screen = pygame.display.set_mode((1200, 800))
end = Ending(screen)
end.run()
pygame.quit()'''