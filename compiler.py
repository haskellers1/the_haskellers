import pygame
import subprocess

pygame.init()
screen_width, screen_height = 1200, 800
screen = pygame.display.set_mode((screen_width, screen_height))

code_font = pygame.font.SysFont('Courier New', 30)  # Adjusted font size

# Command and output history
command_output_history = []
current_command = ""
max_display_lines = 15  # Maximum number of lines visible on screen

# Start GHCi subprocess
ghci_process = subprocess.Popen(['ghci'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

# Read initial output from GHCi (such as version information)
initial_output = ghci_process.stdout.readline().strip()
command_output_history.append(("", initial_output))  # Store initial output in history

# Function to display wrapped code/text with word splitting
def display_code(code, x, y, font_size=30, max_width=1160):
    font = pygame.font.SysFont('Courier New', font_size)
    lines = []
    words = code.split()
    current_line = ''

    for word in words:
        test_line = current_line + ' ' + word if current_line else word
        text_surface = font.render(test_line, True, pygame.Color('green'))

        if text_surface.get_width() <= max_width:
            current_line = test_line
        else:
            # Handle case where word itself is too long to fit in max_width
            if font.size(word)[0] > max_width:
                segments = [word[i:i + 60] for i in range(0, len(word), 60)]
                for segment in segments:
                    lines.append(current_line)
                    current_line = segment.strip()
            else:
                lines.append(current_line.strip())
                current_line = word

    if current_line:
        lines.append(current_line.strip())

    # Render lines in correct order
    for i, line in enumerate(lines):
        if line.strip() == "":
            continue

        text_surface = font.render(line, True, pygame.Color('green'))
        screen.blit(text_surface, (x, y))
        y += text_surface.get_height() + 2  # Use text_surface.get_height() for accurate line spacing

    return y  # Return updated y position


running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                # Execute current command
                ghci_process.stdin.write(current_command + '\n')
                ghci_process.stdin.flush()
                current_output = ghci_process.stdout.readline().strip()

                # Store command and output in history
                command_output_history.append((current_command, current_output))

                # Clear current command
                current_command = ""

                # Adjust history to keep only max_display_lines number of entries
                if len(command_output_history) > max_display_lines:
                    command_output_history = command_output_history[-max_display_lines:]

            elif event.key == pygame.K_BACKSPACE:
                current_command = current_command[:-1]
            else:
                current_command += event.unicode

    screen.fill((0, 0, 0))

    # Calculate where to start rendering history
    if len(command_output_history) > max_display_lines:
        start_index = len(command_output_history) - max_display_lines
    else:
        start_index = 0

    # Render previous commands and outputs
    y = 20
    line_spacing = 5  # Adjust spacing between lines
    for i in range(start_index, len(command_output_history)):
        cmd, out = command_output_history[i]

        # Render command
        cmd_surface = code_font.render("> " + cmd, True, pygame.Color('blue'))
        screen.blit(cmd_surface, (20, y))
        y += cmd_surface.get_height() + line_spacing  # Increase y for next line

        # Render output lines
        lines = out.split('\n')
        for line in lines:
            if line.strip() == "":
                continue
            # Check if line needs wrapping and handle long words
            if code_font.size(line)[0] > 1160:  # Adjust this width as needed
                y = display_code(line, 40, y, font_size=30, max_width=1160)
                y += line_spacing  # Add extra spacing after wrapped lines
            else:
                out_surface = code_font.render(line, True, pygame.Color('green'))
                screen.blit(out_surface, (40, y))
                y += out_surface.get_height() + line_spacing  # Increase y for next line

    # Render current input
    input_surface = code_font.render("> " + current_command, True, pygame.Color('blue'))
    input_y = screen_height - 70  # Adjusted y-coordinate for input line
    screen.blit(input_surface, (20, input_y))

    pygame.display.update()

# Clean up
ghci_process.stdin.close()
ghci_process.stdout.close()
ghci_process.terminate()
pygame.quit()
