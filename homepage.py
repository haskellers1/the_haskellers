import pygame
import sys
import subprocess
from rules import Rules
from terminal_rules import Terminal_Rules
#pygame.init()
from terminal_main import Main



class Button:
    pygame.font.init()
    gui_font = pygame.font.Font(None, 30)

    def __init__(self, text, width, height, pos, elevation):
        self.pressed = False
        self.elevation = elevation
        self.dynamic_elevation = elevation
        self.original_y_pos = pos[1]
        self.text = text

        self.top_rect = pygame.Rect(pos, (width, height))
        self.top_color = '#A75FD2'

        self.bottom_rect = pygame.Rect(pos, (width, height))
        self.bottom_color = '#42135E'

        self.update_text_surface()

    def update_text_surface(self):
        self.text_surf = self.gui_font.render(self.text, True, '#FFFFFF')
        self.text_rect = self.text_surf.get_rect(center=self.top_rect.center)

    def draw(self, screen):
        self.top_rect.y = self.original_y_pos - self.dynamic_elevation
        self.text_rect.center = self.top_rect.center

        self.bottom_rect.midtop = self.top_rect.midtop
        self.bottom_rect.height = self.top_rect.height + self.dynamic_elevation

        pygame.draw.rect(screen, self.bottom_color, self.bottom_rect, border_radius=12)
        pygame.draw.rect(screen, self.top_color, self.top_rect, border_radius=12)
        screen.blit(self.text_surf, self.text_rect)
        return self.check_click()

    def check_click(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.top_rect.collidepoint(mouse_pos):
            self.top_color = '#4F0979'
            if pygame.mouse.get_pressed()[0]:
                self.dynamic_elevation = 0
                self.pressed = True
            else:
                self.dynamic_elevation = self.elevation
                if self.pressed:
                    self.pressed = False
                    return True
        else:
            self.dynamic_elevation = self.elevation
            self.top_color = '#6B169F'
        return False

class Homepage:
    SCREEN_WIDTH = 1200
    SCREEN_HEIGHT = 800

    def __init__(self, screen):
        self.screen = screen
        self.background_image = pygame.image.load('Homepage.png')
        self.background_image = pygame.transform.scale(self.background_image, (self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        self.button1 = Button("<< PRACTICE", 170, 40, (250, 595), 5)
        self.button2 = Button("RULES", 100, 40, (320, 650), 5)
        self.button3 = Button("QUIZZ >>", 170, 40, (770, 595), 5)
        self.button4 = Button("RULES", 100, 40, (770, 650), 5)

    def draw_buttons(self):
        if self.button1.draw(self.screen):
            terminal_game = Main(self.screen)
            terminal_game.run()

        if self.button2.draw(self.screen):
            terminal_rules = Terminal_Rules(self.screen)
            terminal_rules.draw()
            pygame.display.update()
            pygame.time.delay(5000)
        
        if self.button3.draw(self.screen):
            return False

        if self.button4.draw(self.screen):
            rules = Rules(self.screen)
            rules.draw()
            pygame.display.update()
            pygame.time.delay(5000)
        


    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            self.screen.blit(self.background_image, (0, 0))
            if self.draw_buttons() == False:
                return

            pygame.display.flip()
            pygame.time.Clock().tick(60)

        pygame.quit()
        sys.exit()

'''pygame.init()
screen = pygame.display.set_mode((Homepage.SCREEN_WIDTH, Homepage.SCREEN_HEIGHT))
pygame.display.set_caption("Homepage")

homepage = Homepage(screen)
homepage.run()
'''
